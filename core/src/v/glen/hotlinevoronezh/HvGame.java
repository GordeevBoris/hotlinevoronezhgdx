package v.glen.hotlinevoronezh;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import v.glen.hotlinevoronezh.screenery.ScreenStateMachine;

public class HvGame extends Game {

    private ScreenStateMachine screenStateMachine;
    private AssetManager assetManager;
	
	@Override
	public void create () {
        assetManager = new AssetManager();
        linkAllAssetsWithManager();
        screenStateMachine = new ScreenStateMachine(this, assetManager);
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        ShaderProgram.pedantic = false;
	}

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
	public void render () {
        super.render();
	}

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
	public void dispose () {
        super.dispose();
        screenStateMachine.dispose();
        assetManager.dispose();
	}

    private void linkAllAssetsWithManager() {
        assetManager.load("Title.png", Texture.class);
        assetManager.load("Button.png", Texture.class);
        assetManager.load("gg.png", Texture.class);
        assetManager.load("map.png", Texture.class);
        assetManager.load("menu.png", Texture.class);
        assetManager.load("stick.png", Texture.class);
        assetManager.load("sadfrog.png", Texture.class);
        assetManager.load("FloorTexture.png", Texture.class);
        assetManager.load("BlackPixel.png", Texture.class);
        assetManager.load("light.png", Texture.class);
        /*
        Downloaded from http://opengameart.org/content/blood-splats
        and used under CC-BY 3.0 license https://creativecommons.org/licenses/by/3.0/
         */
        assetManager.load("bloodsplats_0003.png", Texture.class);
        /*
        Downloaded from http://opengameart.org/content/pistol-1
        and used under author's ( clayster2012 ) permission
         */
        assetManager.load("pistallx4.png", Texture.class);
    }

}
