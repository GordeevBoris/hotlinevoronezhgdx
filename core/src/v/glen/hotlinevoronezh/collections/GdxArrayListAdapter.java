package v.glen.hotlinevoronezh.collections;

import com.badlogic.gdx.utils.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by Boris on 9/3/2016.
 */
public class GdxArrayListAdapter<T> implements List<T> {

    private final Array<T> array;

    public GdxArrayListAdapter(int capacity, boolean ordered) {
        array = new Array<>(ordered, capacity);
    }

    public GdxArrayListAdapter(List<T> source) {
        array = new Array<>((T[])source.toArray());
    }

    @Override
    public int size() {
        return array.size;
    }

    @Override
    public boolean isEmpty() {
        return array.size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return array.contains((T)o, true);
    }

    @Override
    public Iterator<T> iterator() {
        return array.iterator();
    }

    @Override
    public Object[] toArray() {
        return array.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return (T[])array.items;
    }

    @Override
    public boolean add(T t) {
        array.add(t);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return array.removeValue((T)o, true);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean contains = true;
        for(Object o : c) {
            contains = contains && contains(o);
        }
        return contains;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        array.addAll((T[])c.toArray());
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        array.addAll((T[])c.toArray(), index, c.size());
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for(Object o : c) {
            remove(o);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new NotImplementedException();
    }

    @Override
    public void clear() {
        array.clear();
    }

    @Override
    public T get(int index) {
        return array.get(index);
    }

    @Override
    public T set(int index, T element) {
        T val = array.get(index);
        array.set(index, element);
        return val;
    }

    @Override
    public void add(int index, T element) {
        array.insert(index, element);
    }

    @Override
    public T remove(int index) {
        return array.removeIndex(index);
    }

    @Override
    public int indexOf(Object o) {
        return array.indexOf((T)o, true);
    }

    @Override
    public int lastIndexOf(Object o) {
        return array.lastIndexOf((T)o, true);
    }

    @Override
    public ListIterator<T> listIterator() {
        throw new NotImplementedException();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        throw new NotImplementedException();
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        throw new NotImplementedException();
    }
}
