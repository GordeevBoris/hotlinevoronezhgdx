package v.glen.hotlinevoronezh.logic.controls;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import v.glen.hotlinevoronezh.logic.controls.android.AndroidInputProcessor;

/**
 * Created by noire_000 on 7/18/2016.
 */
public final class CrossPlatformControlsFactory {

    private CrossPlatformControlsFactory() {}

    public static HvInputProcessor create(OrthographicCamera guicamera,
                                          SpriteBatch gui,
                                          AssetManager assetSource) {
        switch (Gdx.app.getType()) {
            case Android:
                return new AndroidInputProcessor(guicamera, gui, assetSource);
            default:
                throw new NotImplementedException();
        }
    }

}
