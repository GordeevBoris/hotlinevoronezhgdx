package v.glen.hotlinevoronezh.logic.controls;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.world.MainCharacter;

/**
 * Created by noire_000 on 7/18/2016.
 */
public interface HvInputProcessor extends InputProcessor {

    void renderGuiElements();

    Double processInputShoot();

    Double processInputMove();

    boolean minimapOn();

    boolean showMenu();

    Vector3 getLastPointer();

    void dispose();

}
