package v.glen.hotlinevoronezh.logic.controls;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by noire_000 on 8/5/2016.
 */
public class Button {

    private final Rectangle rectangle;
    private final String text;

    public Button(Rectangle rectangle, String text) {
        this.rectangle = rectangle;
        this.text = text;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public String getText() {
        return text;
    }
}
