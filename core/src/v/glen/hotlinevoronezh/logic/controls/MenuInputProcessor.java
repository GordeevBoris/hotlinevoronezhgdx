package v.glen.hotlinevoronezh.logic.controls;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import java.util.List;

/**
 * Created by noire_000 on 8/5/2016.
 */
public class MenuInputProcessor implements InputProcessor {

    private List<Rectangle> buttons;
    private Camera guiCamera;
    private int activeButton = 0, pressedButton = 0;
    private Vector3 unprojector;

    public MenuInputProcessor(List<Rectangle> buttons, Camera guiCamera) {
        this.buttons = buttons;
        this.guiCamera = guiCamera;
        this.unprojector = new Vector3(0,0,0);
        activeButton = -1;
        pressedButton = -1;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Vector3 input = guiCamera.unproject(unprojector.set(screenX, screenY, 0));
        for (Rectangle pressed : buttons) {
            if (pressed.contains(input.x, input.y)) {
                activeButton = buttons.indexOf(pressed);
            }
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        Vector3 input = guiCamera.unproject(unprojector.set(screenX, screenY, 0));
        for (Rectangle pressed : buttons) {
            if (pressed.contains(input.x, input.y)) {
                if (activeButton == buttons.indexOf(pressed)) {
                    pressedButton = activeButton;
                }
            }
        }
        activeButton = -1;
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public int getPressedButton() {
        int pressedButton = this.pressedButton;
        this.pressedButton = -1;
        return pressedButton;
    }
}
