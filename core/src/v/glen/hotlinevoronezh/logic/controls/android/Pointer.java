package v.glen.hotlinevoronezh.logic.controls.android;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by noire_000 on 7/18/2016.
 */
public class Pointer {

    private Vector3 position;
    private boolean active;

    public Pointer() {
        position = new Vector3();
        active = false;
    }

    public Vector3 getPosition() {
        return position;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
