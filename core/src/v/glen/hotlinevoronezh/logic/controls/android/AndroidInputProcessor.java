package v.glen.hotlinevoronezh.logic.controls.android;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.controls.HvInputProcessor;
import v.glen.hotlinevoronezh.logic.world.MainCharacter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by noire_000 on 7/18/2016.
 */
public class AndroidInputProcessor implements HvInputProcessor {

    private static final float stickRadius = 100.0f;
    private static final int numPointers = 4;

    private OrthographicCamera guicamera;
    private Vector3 unprojector, movestick, shootstick, lastInput, minimapButton, menuButton;
    private SpriteBatch gui;
    private Texture stick, minimap, menu;
    private List<Pointer> pointers;

    public AndroidInputProcessor(OrthographicCamera guicamera,
                                 SpriteBatch gui,
                                 AssetManager assetSource) {
        this.gui = gui;
        this.guicamera = guicamera;
        stick = assetSource.get("stick.png", Texture.class);
        minimap = assetSource.get("map.png", Texture.class);
        menu = assetSource.get("menu.png", Texture.class);
        unprojector = new Vector3();
        guicamera.position.set(guicamera.viewportWidth / 2f, guicamera.viewportHeight / 2f, 0);
        guicamera.update();

        movestick = new Vector3(200, 200, 0);
        shootstick = new Vector3(guicamera.viewportWidth - 200, 200, 0);
        minimapButton = new Vector3(guicamera.viewportWidth - 200, guicamera.viewportHeight - 200, 0);
        menuButton = new Vector3(guicamera.viewportWidth - 200, guicamera.viewportHeight - 500, 0);
        lastInput = new Vector3(0,0,0);

        pointers = new ArrayList<>();
        for (int i = 0; i < numPointers; i++) {
            pointers.add(new Pointer());
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Vector3 input = guicamera.unproject(unprojector.set(screenX, screenY, 0));
        if (pointer < numPointers) {
            Pointer touched = pointers.get(pointer);
            touched.getPosition().set(input);
            touched.setActive(true);
            lastInput.set(input);
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (pointer < numPointers) {
            pointers.get(pointer).setActive(false);
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        Vector3 input = guicamera.unproject(unprojector.set(screenX, screenY, 0));
        if (pointer < numPointers) {
            pointers.get(pointer).getPosition().set(input);
        }
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void renderGuiElements() {
        gui.draw(stick, movestick.x - stickRadius, movestick.y - stickRadius, 200, 200);
        gui.draw(stick, shootstick.x - stickRadius, shootstick.y - stickRadius, 200, 200);
        gui.draw(minimap, minimapButton.x - stickRadius, minimapButton.y - stickRadius, 200, 200);
        gui.draw(menu, menuButton.x - stickRadius, menuButton.y - stickRadius, 200, 200);
    }

    @Override
    public Double processInputMove() {
        return guiElementTouched(movestick, 200);
    }

    @Override
    public Double processInputShoot() {
        return guiElementTouched(shootstick, 200);
    }

    @Override
    public boolean minimapOn() {
        return guiElementTouched(minimapButton, 200) != null;
    }

    @Override
    public boolean showMenu() {
        return guiElementTouched(menuButton, 200) != null;
    }

    private Double guiElementTouched(Vector3 middle, int distance) {
        for (Pointer pointer : pointers) {
            if (pointer.isActive()) {
                Vector3 input = pointer.getPosition();
                if (input.dst(middle) < distance) {
                    double angle = Math.toDegrees(
                            Math.atan2(input.y - middle.y, input.x - middle.x)
                    );
                    if (angle < 0) {
                        angle += 360;
                    }
                    return angle;
                }
            }
        }
        return null;
    }

    @Override
    public void dispose() {
    }

    @Override
    public Vector3 getLastPointer() {
        return lastInput;
    }

}
