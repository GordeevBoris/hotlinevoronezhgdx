package v.glen.hotlinevoronezh.logic.controls;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by noire_000 on 8/6/2016.
 */
public class ButtonGradient extends Sprite {

    public ButtonGradient(TextureRegion white) {
        setRegion(white);
    }

    public void setGradientColor(Color a, Color b, boolean horizontal) {
        float[] vertices = getVertices();
        float ca = a.toFloatBits();
        float cb = b.toFloatBits();
        vertices[SpriteBatch.C1] = horizontal ? ca : cb;
        vertices[SpriteBatch.C2] = ca;
        vertices[SpriteBatch.C3] = horizontal ? cb : ca;
        vertices[SpriteBatch.C4] = cb;
    }
}
