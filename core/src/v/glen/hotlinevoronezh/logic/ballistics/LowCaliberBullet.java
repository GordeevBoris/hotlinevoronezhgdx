package v.glen.hotlinevoronezh.logic.ballistics;

import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.generation.Cell;

import java.util.List;

/**
 * Created by noire_000 on 7/31/2016.
 */
public class LowCaliberBullet extends Projectile {

    private static final ITrajectoryMethod TRAJECTORY_METHOD =
            ITrajectoryMethod.NON_BOUNCY_TRAJECTORY_METHOD;

    public LowCaliberBullet(
            Cell activeRoom,
            double speed,
            double degreeHeading,
            double damage,
            Vector3 position
    ) {
        super(activeRoom,
                speed,
                degreeHeading,
                damage,
                position,
                100);
    }

    @Override
    public ProjectileFrameResult frame(List<Cell> cells) {
        ProjectileFrameResult result =
                TRAJECTORY_METHOD.calculateFrameTrajectory(activeRoom,
                        speed,
                        angleHeading,
                        position,
                        cells);
        if (result.getLastLine().getEndCollisionType() ==
                TrajectoryCollisionType.END_OF_FRAME) {
            speed = result.getResultSpeed();
            angleHeading = result.getResultDegreeHeading();
            setActiveRoom(result.getFinalCell());
            position.set(result.getFinalPosition().x, result.getFinalPosition().y, result.getFinalPosition().z);
        }
        return result;
    }
}
