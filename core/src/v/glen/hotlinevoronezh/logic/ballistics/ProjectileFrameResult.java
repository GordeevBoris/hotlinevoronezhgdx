package v.glen.hotlinevoronezh.logic.ballistics;

import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.generation.Cell;

import java.util.List;

/**
 * Created by noire_000 on 7/31/2016.
 */
public class ProjectileFrameResult {

    private List<TrajectoryLine> lines;
    private Vector3 finalPosition;
    private double resultSpeed;
    private double resultDegreeHeading;
    private Cell finalCell;

    public ProjectileFrameResult(
            List<TrajectoryLine> lines,
            Vector3 finalPosition,
            double resultSpeed,
            double resultDegreeHeading,
            Cell finalCell
    ) {
        this.lines = lines;
        this.finalPosition = finalPosition;
        this.resultSpeed = resultSpeed;
        this.resultDegreeHeading = resultDegreeHeading;
        this.finalCell = finalCell;
    }

    public List<TrajectoryLine> getLines() {
        return lines;
    }

    public TrajectoryLine getLastLine() {
        return lines.get(lines.size() - 1);
    }

    public Vector3 getFinalPosition() {
        return finalPosition;
    }

    public double getResultSpeed() {
        return resultSpeed;
    }

    public double getResultDegreeHeading() {
        return resultDegreeHeading;
    }

    public Cell getFinalCell() {
        return finalCell;
    }

    public void setLines(List<TrajectoryLine> lines) {
        this.lines = lines;
    }

    public void setFinalPosition(Vector3 finalPosition) {
        this.finalPosition = finalPosition;
    }

    public void setResultSpeed(double resultSpeed) {
        this.resultSpeed = resultSpeed;
    }

    public void setResultDegreeHeading(double resultDegreeHeading) {
        this.resultDegreeHeading = resultDegreeHeading;
    }

    public void setFinalCell(Cell finalCell) {
        this.finalCell = finalCell;
    }

    public double getTotalDistancePassed() {
        double distancePassed = 0d;
        for (TrajectoryLine line : lines) {
            distancePassed += Math.sqrt(
                    (line.getEnd().x - line.getStart().x) * (line.getEnd().x - line.getStart().x)
                            + (line.getEnd().y - line.getStart().y) * (line.getEnd().y - line.getStart().y)
            );
        }
        return distancePassed;
    }
}
