package v.glen.hotlinevoronezh.logic.ballistics;

import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.generation.Cell;

/**
 * Created by noire_000 on 7/31/2016.
 */
public class TrajectoryLine {

    private final Vector3 start, end;
    private TrajectoryCollisionType endCollisionType;
    private Object endCollisionObject;
    private final Cell endCell, startCell;

    public TrajectoryLine(Vector3 start, Vector3 end,
                          TrajectoryCollisionType endCollisionType,
                          Object endCollisionObject,
                          Cell endCell,
                          Cell startCell) {
        this.start = start;
        this.end = end;
        this.endCollisionType = endCollisionType;
        this.endCollisionObject = endCollisionObject;
        this.endCell = endCell;
        this.startCell = startCell;
    }

    public Vector3 getStart() {
        return start;
    }

    public Vector3 getEnd() {
        return end;
    }

    public TrajectoryCollisionType getEndCollisionType() {
        return endCollisionType;
    }

    public Object getEndCollisionObject() {
        return endCollisionObject;
    }

    public Cell getEndCell() {
        return endCell;
    }

    public void setEndCollisionType(TrajectoryCollisionType endCollisionType) {
        this.endCollisionType = endCollisionType;
    }

    public Cell getStartCell() {
        return startCell;
    }

    public void setEndCollisionObject(Object endCollisionObject) {
        this.endCollisionObject = endCollisionObject;
    }
}
