package v.glen.hotlinevoronezh.logic.ballistics;

/**
 * Created by noire_000 on 7/31/2016.
 */
public enum TrajectoryCollisionType {
    START_OF_FRAME,
    END_OF_FRAME,
    BOUNCE,
    PENETRATION,
    DESTRUCTION,
    CELL_CHANGE
}
