package v.glen.hotlinevoronezh.logic.ballistics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.collections.GdxArrayListAdapter;
import v.glen.hotlinevoronezh.logic.generation.Cell;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Boris on 8/23/2016.
 */
public class SlipperyTrajectoryMethod implements ITrajectoryMethod {

    private static final NonBouncyTrajectoryMethod NON_BOUNCY_TRAJECTORY
            = ITrajectoryMethod.NON_BOUNCY_TRAJECTORY_METHOD;

    protected SlipperyTrajectoryMethod(){}

    private final ProjectileFrameResult resultInternal = new ProjectileFrameResult(
            new GdxArrayListAdapter<TrajectoryLine>(10, true),
            new Vector3(0,0,0),
            0,
            0,
            null
    );

    @Override
    public ProjectileFrameResult calculateFrameTrajectory(
            Cell activeRoom,
            double speed,
            double angleHeading,
            Vector3 startingPosition,
            List<Cell> cells
    ) {
        resultInternal.getLines().clear();
        resultInternal.setResultDegreeHeading(angleHeading);
        resultInternal.setResultSpeed(speed);
        NON_BOUNCY_TRAJECTORY.calculateFrameTrajectory(
                activeRoom,
                speed,
                angleHeading,
                startingPosition,
                cells,
                resultInternal
        );
        if (resultInternal.getLastLine().getEndCollisionType()
                == TrajectoryCollisionType.DESTRUCTION
                && !floatEquals((float)angleHeading, 0)
                && !floatEquals((float)angleHeading, 90)
                && !floatEquals((float)angleHeading, 180)
                && !floatEquals((float)angleHeading, 270)
                && !floatEquals((float)angleHeading, 360)) {
            resultInternal.getLastLine().setEndCollisionType(TrajectoryCollisionType.BOUNCE);
            double distancePassed = resultInternal.getTotalDistancePassed(),
                    resultAngle, resultSpeed;
            if ((floatEquals(resultInternal.getFinalPosition().x, resultInternal.getFinalCell().getRectangle().x)
                    && (angleHeading > 90 && angleHeading < 270))
                    || (floatEquals(resultInternal.getFinalPosition().x, resultInternal.getFinalCell().getRectangle().x
                        + resultInternal.getFinalCell().getRectangle().width) && (angleHeading < 90 || angleHeading > 270))) {
                if (angleHeading > 0 && angleHeading < 180) {
                    resultAngle = 90;
                } else {
                    resultAngle = 270;
                }
                resultSpeed = Math.abs(Math.sin(Math.toRadians(angleHeading)) * (speed - distancePassed));
            } else {
                if (angleHeading < 90 || angleHeading > 270) {
                    resultAngle = 0;
                } else {
                    resultAngle = 180;
                }
                resultSpeed = Math.abs(Math.cos(Math.toRadians(angleHeading)) * (speed - distancePassed));
            }
            NON_BOUNCY_TRAJECTORY.calculateFrameTrajectory(
                    resultInternal.getFinalCell(),
                    resultSpeed,
                    resultAngle,
                    resultInternal.getFinalPosition(),
                    cells,
                    resultInternal
            );
        }
        return resultInternal;
    }

    private boolean floatEquals(float v1, float v2) {
        return Math.abs(v1 - v2) < 0.01;
    }

}
