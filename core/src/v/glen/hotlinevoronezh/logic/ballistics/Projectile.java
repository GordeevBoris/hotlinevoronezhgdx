package v.glen.hotlinevoronezh.logic.ballistics;

import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.generation.Cell;

import java.util.List;

/**
 * Created by noire_000 on 7/31/2016.
 */
public abstract class Projectile {

    private static long globalId = 0;

    protected static final ITrajectoryMethod DEFAULT_TRAJECTORY_METHOD
            = new NonBouncyTrajectoryMethod();

    protected Cell activeRoom;
    protected double speed, angleHeading, damage;
    protected Vector3 position;
    protected int framesAlive;

    private long id;

    protected Projectile(Cell activeRoom,
                         double speed,
                         double angleHeading,
                         double damage,
                         Vector3 position,
                         int framesAlive) {
        this.activeRoom = activeRoom;
        this.speed = speed;
        this.position = position;
        this.angleHeading = angleHeading;
        this.damage = damage;
        this.id = globalId;
        this.framesAlive = framesAlive;
        globalId++;
    }

    public abstract ProjectileFrameResult frame(List<Cell> cells);

    public void destroy() {
        this.activeRoom.getProjectiles().remove(this);
    }

    public Cell getActiveRoom() {
        return activeRoom;
    }

    public Vector3 getPosition() {
        return position;
    }

    public double getSpeed() {
        return speed;
    }

    public double getAngleHeading() {
        return angleHeading;
    }

    protected void setActiveRoom(Cell activeRoom) {
        this.activeRoom.getProjectiles().remove(this);
        this.activeRoom = activeRoom;
        this.activeRoom.getProjectiles().add(this);
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object that) {
        return  (Projectile.class.isAssignableFrom(that.getClass()) &&
                ((Projectile)that).getId() == this.getId());
    }

    public int getFramesAlive() {
        return framesAlive;
    }

    public void setFramesAlive(int framesAlive) {
        this.framesAlive = framesAlive;
    }

    public double getDamage() {
        return damage;
    }
}
