package v.glen.hotlinevoronezh.logic.ballistics;

import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.collections.GdxArrayListAdapter;
import v.glen.hotlinevoronezh.logic.generation.Cell;

import java.util.List;

/**
 * Created by noire_000 on 7/31/2016.
 */
public class NonBouncyTrajectoryMethod implements ITrajectoryMethod {

    private final ProjectileFrameResult resultInternal = new ProjectileFrameResult(
            new GdxArrayListAdapter<TrajectoryLine>(10, true),
            new Vector3(0,0,0),
            0,
            0,
            null
    );

    protected NonBouncyTrajectoryMethod(){}

    @Override
    public ProjectileFrameResult calculateFrameTrajectory(
            Cell activeRoom,
            double speed,
            double angleHeading,
            Vector3 startingPosition,
            List<Cell> cells
    ) {
        resultInternal.getLines().clear();
        resultInternal.setResultDegreeHeading(angleHeading);
        resultInternal.setResultSpeed(speed);
        calculateFrameTrajectory(
                activeRoom,
                speed,
                angleHeading,
                startingPosition,
                cells,
                resultInternal
        );
        return resultInternal;
    }

    public void calculateFrameTrajectory(Cell activeRoom,
                                         double speed,
                                         double angleHeading,
                                         Vector3 startingPosition,
                                         List<Cell> cells,
                                         ProjectileFrameResult result) {
        Vector3 startingPositionCopy = new Vector3(startingPosition.x, startingPosition.y, 0);
        Cell resultCell = processLine(activeRoom,
                speed,
                angleHeading,
                startingPositionCopy,
                result.getLines(),
                0
        );
        result.setFinalPosition(result.getLastLine().getEnd());
        result.setFinalCell(resultCell);
    }

    private Cell processLine(
            Cell activeRoom,
            double speed,
            double angleHeading,
            Vector3 startingPosition,
            List<TrajectoryLine> lines,
            int depth
    ) {
        if (depth > 100) {
            return activeRoom;
        } else {
            depth++;
        }
        double minDistance = Double.MAX_VALUE;
        Vector3 point2 = null;
        if (angleHeading < 90 || angleHeading > 270) {
            Vector3 point2_1 = findFunctionsCrossing(startingPosition, false,
                    activeRoom.getRectangle().x + activeRoom.getRectangle().width,
                    angleHeading);
            double distance = Math.sqrt(
                    (startingPosition.x - point2_1.x) * (startingPosition.x - point2_1.x) +
                            (startingPosition.y - point2_1.y) * (startingPosition.y - point2_1.y));
            if (distance < minDistance) {
                minDistance = distance;
                point2 = point2_1;
            }
        }
        if (angleHeading < 270 && angleHeading > 90) {
            Vector3 point2_2 = findFunctionsCrossing(startingPosition, false,
                    activeRoom.getRectangle().x,
                    angleHeading);
            double distance = Math.sqrt(
                    (startingPosition.x - point2_2.x) * (startingPosition.x - point2_2.x) +
                            (startingPosition.y - point2_2.y) * (startingPosition.y - point2_2.y));
            if (distance < minDistance) {
                minDistance = distance;
                point2 = point2_2;
            }
        }
        if (angleHeading < 180 && angleHeading > 0) {
            Vector3 point2_3 = findFunctionsCrossing(startingPosition, true,
                    activeRoom.getRectangle().y + activeRoom.getRectangle().height,
                    angleHeading);
            double distance = Math.sqrt(
                    (startingPosition.x - point2_3.x) * (startingPosition.x - point2_3.x) +
                            (startingPosition.y - point2_3.y) * (startingPosition.y - point2_3.y));
            if (distance < minDistance) {
                minDistance = distance;
                point2 = point2_3;
            }
        }
        if (angleHeading < 360 && angleHeading > 180) {
            Vector3 point2_4 = findFunctionsCrossing(startingPosition, true,
                    activeRoom.getRectangle().y,
                    angleHeading);
            double distance = Math.sqrt(
                    (startingPosition.x - point2_4.x) * (startingPosition.x - point2_4.x) +
                            (startingPosition.y - point2_4.y) * (startingPosition.y - point2_4.y));
            if (distance < minDistance) {
                minDistance = distance;
                point2 = point2_4;
            }
        }
        if (point2 == null) {
            return activeRoom;
        }
        if (minDistance > speed) {
            double coefficient = speed / minDistance,
                    x3 = startingPosition.x + (point2.x - startingPosition.x) * coefficient,
                    y3 = startingPosition.y + (point2.y - startingPosition.y) * coefficient;
            Vector3 endPoint = new Vector3((float)x3, (float)y3, 0f);
            lines.add(new TrajectoryLine(startingPosition, endPoint,
                    TrajectoryCollisionType.END_OF_FRAME, null, activeRoom,
                    activeRoom));
            return activeRoom;
        } else {
            Cell nextCell = null;
            for (Cell adjacent : activeRoom.getAdjacent()) {
                if (belongs(adjacent, point2)) {
                    nextCell = adjacent;
                    break;
                }
            }
            if (nextCell == null) {
                lines.add(new TrajectoryLine(startingPosition, point2,
                        TrajectoryCollisionType.DESTRUCTION, activeRoom, activeRoom,
                        activeRoom));
                return activeRoom;
            } else {
                lines.add(new TrajectoryLine(startingPosition, point2,
                        TrajectoryCollisionType.END_OF_FRAME, activeRoom, nextCell,
                        activeRoom));
                speed -= minDistance;
                return processLine(
                        nextCell,
                        speed,
                        angleHeading,
                        point2,
                        lines,
                        depth
                );
            }
        }
    }

    private boolean belongs(Cell adjacent, Vector3 point) {
        return ((adjacent.getRectangle().getX() - 0.001) < point.x &&
                (adjacent.getRectangle().getX() + adjacent.getRectangle().getWidth() + 0.001) > point.x &&
                (adjacent.getRectangle().getY() - 0.001) < point.y &&
                (adjacent.getRectangle().getY() + adjacent.getRectangle().getHeight() + 0.001) > point.y);
    }

    private Vector3 findFunctionsCrossing(
            Vector3 point1,
            boolean flipXY,
            double x2,
            double ang1
    ) {
        double m = Math.tan(Math.toRadians(ang1)),
                n = point1.y - m * point1.x;
        if (flipXY) {
            n *= -1 / m;
            m = 1 / m;
        }
        double y2 = m * x2 + n;
        Vector3 crossing = new Vector3(Vector3.Zero);
        if (flipXY) {
            crossing.x = (float)y2;
            crossing.y = (float)x2;
        } else {
            crossing.x = (float)x2;
            crossing.y = (float)y2;
        }
        return crossing;
    }

}
