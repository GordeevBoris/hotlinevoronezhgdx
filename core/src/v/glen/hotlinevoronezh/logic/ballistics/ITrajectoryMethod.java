package v.glen.hotlinevoronezh.logic.ballistics;

import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.generation.Cell;

import java.util.List;

/**
 * Created by noire_000 on 7/31/2016.
 */
public interface ITrajectoryMethod {

    NonBouncyTrajectoryMethod NON_BOUNCY_TRAJECTORY_METHOD
            = new NonBouncyTrajectoryMethod();
    ITrajectoryMethod BOUNCY_TRAJECTORY_METHOD
            = new BouncyTrajectoryMethod();
    ITrajectoryMethod SLIPPERY_TRAJECTORY_METHOD
            = new SlipperyTrajectoryMethod();

    ProjectileFrameResult calculateFrameTrajectory(
            Cell activeRoom,
            double speed,
            double angleHeading,
            Vector3 startingPosition,
            List<Cell> cells
    );

}
