package v.glen.hotlinevoronezh.logic.ballistics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.collections.GdxArrayListAdapter;
import v.glen.hotlinevoronezh.logic.generation.Cell;
import v.glen.hotlinevoronezh.logic.generation.Field;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Boris on 8/14/2016.
 */
public class BouncyTrajectoryMethod implements ITrajectoryMethod {

    private static final NonBouncyTrajectoryMethod NON_BOUNCY_TRAJECTORY =
            ITrajectoryMethod.NON_BOUNCY_TRAJECTORY_METHOD;

    protected BouncyTrajectoryMethod(){}

    private final ProjectileFrameResult resultInternal = new ProjectileFrameResult(
            new GdxArrayListAdapter<TrajectoryLine>(10, true),
            new Vector3(0,0,0),
            0,
            0,
            null
    );

    @Override
    public ProjectileFrameResult calculateFrameTrajectory(Cell activeRoom,
                                                          double speed,
                                                          double angleHeading,
                                                          Vector3 startingPosition,
                                                          List<Cell> cells) {
        resultInternal.getLines().clear();
        resultInternal.setResultDegreeHeading(angleHeading);
        resultInternal.setResultSpeed(speed);
        double remainingSpeed = speed, remainingAngle = angleHeading;
        while (remainingSpeed > 0) {
            NON_BOUNCY_TRAJECTORY.calculateFrameTrajectory(
                    activeRoom,
                    remainingSpeed,
                    remainingAngle,
                    startingPosition,
                    cells,
                    resultInternal
            );
            if (resultInternal.getLastLine().getEndCollisionType()
                    != TrajectoryCollisionType.DESTRUCTION) {
                break;
            } else {
                resultInternal.getLastLine().setEndCollisionType(
                        TrajectoryCollisionType.BOUNCE);
                remainingSpeed = speed - resultInternal.getTotalDistancePassed();
                if (doubleEquals(remainingSpeed, 0) || remainingSpeed < 0) {
                    break;
                }
                activeRoom = resultInternal.getFinalCell();
                startingPosition = resultInternal.getFinalPosition();
                if (doubleEquals(startingPosition.x, activeRoom.getRectangle().x)) {
                    remainingAngle += (180 - remainingAngle) * 2;
                    remainingAngle += 180;
                    remainingAngle -= remainingAngle > 360 ? 360 : 0;
                }
                if (doubleEquals(startingPosition.x, activeRoom.getRectangle().x + activeRoom.getRectangle().width)) {
                    remainingAngle = 360 - remainingAngle;
                    remainingAngle += 180;
                    remainingAngle -= remainingAngle > 360 ? 360 : 0;
                }
                if (doubleEquals(startingPosition.y, activeRoom.getRectangle().y)) {
                    remainingAngle += (270 - remainingAngle) * 2;
                    remainingAngle += 180;
                    remainingAngle -= remainingAngle > 360 ? 360 : 0;
                }
                if (doubleEquals(startingPosition.y, activeRoom.getRectangle().y + activeRoom.getRectangle().height)) {
                    remainingAngle += (90 - remainingAngle)*2;
                    remainingAngle += 180;
                    remainingAngle -= remainingAngle > 360 ? 360 : 0;
                }
                resultInternal.setResultDegreeHeading(remainingAngle);
            }
        }
        return resultInternal;
    }

    private boolean doubleEquals(double f1, double f2) {
        return Math.abs(f1 - f2) < 0.1;
    }
}
