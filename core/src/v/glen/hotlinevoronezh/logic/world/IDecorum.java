package v.glen.hotlinevoronezh.logic.world;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Boris on 8/19/2016.
 */
public interface IDecorum {

    void render(SpriteBatch batch, AssetManager manager);

    void renderOcclusive(SpriteBatch batch, AssetManager manager);

    boolean isOcclusive();

}
