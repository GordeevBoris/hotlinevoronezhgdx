package v.glen.hotlinevoronezh.logic.world;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.collections.GdxArrayListAdapter;
import v.glen.hotlinevoronezh.logic.ballistics.Projectile;
import v.glen.hotlinevoronezh.logic.ballistics.ProjectileFrameResult;
import v.glen.hotlinevoronezh.logic.ballistics.TrajectoryCollisionType;
import v.glen.hotlinevoronezh.logic.ballistics.TrajectoryLine;
import v.glen.hotlinevoronezh.logic.generation.*;
import v.glen.hotlinevoronezh.logic.world.weaponry.Pistol;

import java.util.*;

/**
 * Created by noire_000 on 8/1/2016.
 */
public class World {

    private static final Integer runSeed = -341382330;

    private final List<Projectile> projectiles;
    private final List<Cell> cells;
    private final List<Adversary> adversaries;
    private final ParkMillerCartaRng rng;
    private final MainCharacter mainCharacter;

    public static World generateWorld(AssetManager manager) {
        ParkMillerCartaRng rng;
        Vector3 position = new Vector3(0, 0, 0);

        if (runSeed != null) {
            rng = new ParkMillerCartaRng(runSeed);
        } else {
            rng = new ParkMillerCartaRng();
        }
        Queue<ICellEngineState> Program = new ArrayDeque<>();
        Program.add(new SpawnState(rng));
        Program.add(new SeparationState());
        Program.add(new LinkingState());
        Program.add(new FloatingGraphRemovalState(150));
        Program.add(new GrowthState());
        Program.add(new WallsAdditionStage(position));
        CellEngineStrategy strategy = new CellEngineStrategy(Program);

        while (!strategy.isDone()) {
            strategy.tick();
        }

        Cell activeRoom = null;
        for (Cell cell : strategy.getCells()) {
            if (cell.getRectangle().contains(position.x,position.y)) {
                activeRoom = cell;
                break;
            }
        }
        if (activeRoom == null) {
            throw new GenerationException("Character not in room");
        }

        MainCharacter mainCharacter = new MainCharacter(position, activeRoom, 10,
                manager.get("gg.png", Texture.class));

        List<Adversary> adversaries = new ArrayList<>(1000);

        for (int i = 0; i < 1000; i++) {
            int roomNum = (int)rng.generateRanged(0, strategy.getCells().size() - 1);
            Cell room = strategy.getCells().get(roomNum);

            while (room.getId() == mainCharacter.getActiveCell().getId()) {
                roomNum = (int)rng.generateRanged(0, strategy.getCells().size() - 1);
                room = strategy.getCells().get(roomNum);
            }

            SadFrog adversary = new SadFrog(1, 5, new Vector3(
                    room.getRectangle().getCenter(Vector2.Zero).x,
                    room.getRectangle().getCenter(Vector2.Zero).y,
                    0
            ), room, mainCharacter);
            room.getAdversaries().add(adversary);
            adversaries.add(adversary);
        }

        addWeaponsToCells(strategy, manager);

        return new World(strategy.getCells(), rng, mainCharacter, adversaries);
    }

    private static void addWeaponsToCells(CellEngineStrategy strategy,
                                          AssetManager manager) {
        TextureRegion pistol =
                new TextureRegion(manager.get("pistallx4.png", Texture.class));
        pistol.setRegion(0, 101, 163, 101);
        pistol.flip(true, false);

        for (Cell cell : strategy.getCells()) {
            cell.getDecorum().add(new Pistol(pistol, pistol, new Vector3(
                    cell.getRectangle().x + cell.getRectangle().width/2f,
                    cell.getRectangle().y + cell.getRectangle().height/2f,
                    0
            )));
        }
    }

    private World(
            List<Cell> cells,
            ParkMillerCartaRng rng,
            MainCharacter mainCharacter,
            List<Adversary> adversaries
    ) {
        projectiles = new ArrayList<>(100);
        this.cells = new GdxArrayListAdapter<>(cells);
        this.rng = rng;
        this.mainCharacter = mainCharacter;
        this.adversaries = adversaries;
    }

    public void processAdversaries() {
        for (Iterator<Adversary> iterator = adversaries.iterator(); iterator.hasNext();) {
            Adversary adversary = iterator.next();
            if (adversary.getHealth() <= 0) {
                adversary.destroy();
                iterator.remove();
            } else {
                adversary.update();
                if (Math.sqrt(
                        (adversary.getPosition().x - mainCharacter.getPosition().x) * (adversary.getPosition().x - mainCharacter.getPosition().x)
                        + (adversary.getPosition().y - mainCharacter.getPosition().y) * (adversary.getPosition().y - mainCharacter.getPosition().y)
                        ) < adversary.getRadius()){
                    mainCharacter.setHealth(mainCharacter.getHealth() - 1);
                }
            }
        }
    }

    public void processProjectiles() {
        for (Iterator<Projectile> iterator = projectiles.iterator(); iterator.hasNext();) {
            Projectile projectile = iterator.next();
            ProjectileFrameResult result = projectile.frame(cells);
            projectile.setFramesAlive(projectile.getFramesAlive() - 1);
            boolean collision = false;
            for (Adversary adversary : result.getLastLine().getStartCell().getAdversaries()) {
                if (collisionCheck(result, adversary)) {
                    collision = true;
                    adversary.setHealth(adversary.getHealth() - projectile.getDamage());
                    result.getLastLine().setEndCollisionType(TrajectoryCollisionType.DESTRUCTION);
                    result.getLastLine().setEndCollisionObject(adversary);
                    break;
                }
            }
            if (!collision) {
                for (Adversary adversary : result.getLastLine().getEndCell().getAdversaries()) {
                    if (collisionCheck(result, adversary)) {
                        adversary.setHealth(adversary.getHealth() - projectile.getDamage());
                        result.getLastLine().setEndCollisionType(TrajectoryCollisionType.DESTRUCTION);
                        result.getLastLine().setEndCollisionObject(adversary);
                        break;
                    }
                }
            }
            if (result.getLastLine().getEndCollisionType()
                    == TrajectoryCollisionType.DESTRUCTION || projectile.getFramesAlive() <= 0) {
                projectile.destroy();
                iterator.remove();
            }
        }
    }

    public boolean collisionCheck(ProjectileFrameResult result, Adversary adversary) {
        for (Iterator<TrajectoryLine> iterator = result.getLines().iterator(); iterator.hasNext();) {
            TrajectoryLine line = iterator.next();
            if (adversary.getCell().getRectangle().contains(line.getStart().x, line.getStart().y)
                    || adversary.getCell().getRectangle().contains(line.getEnd().x, line.getEnd().y)) {
                if (Math.sqrt(
                        (adversary.getPosition().x - line.getEnd().x) * (adversary.getPosition().x - line.getEnd().x)
                        + (adversary.getPosition().y - line.getEnd().y) * (adversary.getPosition().y - line.getEnd().y)
                ) < adversary.getRadius()) {
                    removeFurtherLines(iterator);
                    return true;
                }
                double m, n, m2, n2, x, y;
                if (line.getEnd().x != line.getStart().x) {
                    m = (line.getEnd().y - line.getStart().y) / (line.getEnd().x - line.getStart().x);
                    n = line.getEnd().y - line.getEnd().x * m;
                    m2 = (Math.pow(m, -1)) * -1;
                } else {
                    n = line.getEnd().x;
                    m2 = 0;
                    m = Float.POSITIVE_INFINITY;
                }
                n2 = adversary.getPosition().y - adversary.getPosition().x * m2;
                x = (n2 - n) / (m - m2);
                y = x * m2 + n2;
                if (
                        ((y > line.getStart().y && y < line.getEnd().y) || (y < line.getStart().y && y > line.getEnd().y))
                        && Math.sqrt(
                                (adversary.getPosition().x - x) * (adversary.getPosition().x - x)
                                        + (adversary.getPosition().y - y) * (adversary.getPosition().y - y)
                        ) < adversary.getRadius()
                ) {
                    removeFurtherLines(iterator);
                    return true;
                }
            }
        }
        return false;
    }

    private void removeFurtherLines(Iterator<TrajectoryLine> iterator) {
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
    }

    public List<Projectile> getProjectiles() {
        return projectiles;
    }

    public List<Cell> getCells() {
        return cells;
    }

    public static Integer getRunSeed() {
        return runSeed;
    }

    public ParkMillerCartaRng getRng() {
        return rng;
    }

    public MainCharacter getMainCharacter() {
        return mainCharacter;
    }

    public List<Adversary> getAdversaries() {
        return adversaries;
    }


}
