package v.glen.hotlinevoronezh.logic.world.weaponry;

import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.world.IDecorum;
import v.glen.hotlinevoronezh.logic.world.MainCharacter;

/**
 * Created by Boris on 9/6/2016.
 */
public interface IWeaponPickup extends IDecorum {

    float PICKUP_RADIUS = 30f;

    Vector3 getPosition();

    IWeapon pickup(MainCharacter character);

}
