package v.glen.hotlinevoronezh.logic.world.weaponry;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.ballistics.Projectile;
import v.glen.hotlinevoronezh.logic.world.MainCharacter;

import java.util.List;

/**
 * Created by Boris on 9/6/2016.
 */
public abstract class AbstractWeapon implements IWeapon, IWeaponPickup {

    protected int ammo, cooldown, cooldownLeft;
    protected MainCharacter owner;
    protected WeaponType weaponType;
    protected Vector3 position;
    protected final TextureRegion weaponTexture, decorumTexture;

    protected AbstractWeapon(TextureRegion decorumTexture, TextureRegion weaponTexture,
                             int ammo, int cooldown, WeaponType weaponType,
                             Vector3 position) {
        this.decorumTexture = decorumTexture;
        this.weaponTexture = weaponTexture;
        this.ammo = ammo;
        this.cooldown = cooldown;
        cooldownLeft = 0;
        this.weaponType = weaponType;
        this.position = position;
    }

    @Override
    public Projectile shoot(List<Projectile> globalProjectiles, double angleOfFire) {
        if (--cooldownLeft <= 0 && ammo > 0) {
            if (owner == null) {
                throw new NoOwnerException();
            }

            Projectile projectile = produceProjectile(angleOfFire);
            globalProjectiles.add(projectile);
            owner.getActiveCell().getProjectiles().add(projectile);
            cooldownLeft = cooldown;
            ammo--;

            return projectile;
        } else {
            return null;
        }
    }

    protected abstract Projectile produceProjectile(double angleOfFire);

    @Override
    public WeaponType getWeaponType() {
        return weaponType;
    }

    @Override
    public IWeapon pickup(MainCharacter character) {
        owner = character;
        character.setWeapon(this);
        return this;
    }

    @Override
    public void render(SpriteBatch batch, AssetManager manager) {
        batch.draw(decorumTexture,
                position.x - decorumTexture.getRegionWidth()/2f,
                position.y - decorumTexture.getRegionHeight()/2f,
                decorumTexture.getRegionWidth(),
                decorumTexture.getRegionHeight());
    }

    @Override
    public void renderOcclusive(SpriteBatch batch, AssetManager manager) {}

    @Override
    public boolean isOcclusive() {
        return false;
    }

    @Override
    public void renderWeapon(SpriteBatch batch, double angle) {
        float rad = (float)Math.toRadians(angle),
                x = owner.getPosition().x + IWeapon.DRAWING_OFFSET * (float)Math.cos(rad),
                y = owner.getPosition().y + IWeapon.DRAWING_OFFSET * (float)Math.sin(rad);
        batch.draw(weaponTexture, x, y,
                0, 0,
                weaponTexture.getRegionWidth(), weaponTexture.getRegionHeight(),
                1, 1,
                (float)angle);
    }

    @Override
    public int getAmmo() {
        return ammo;
    }

    @Override
    public Vector3 getPosition() {
        return position;
    }
}
