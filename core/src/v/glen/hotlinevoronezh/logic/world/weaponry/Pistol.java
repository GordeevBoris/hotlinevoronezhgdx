package v.glen.hotlinevoronezh.logic.world.weaponry;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.ballistics.LowCaliberBullet;
import v.glen.hotlinevoronezh.logic.ballistics.Projectile;

/**
 * Created by Boris on 9/8/2016.
 */
public class Pistol extends AbstractWeapon {

    private static final int AMMO = 15;
    private static final int COOLDOWN = 5;
    private static final double DAMAGE = 10d;
    private static final double SPEED = 30d;
    private static final WeaponType WEAPON_TYPE = WeaponType.PISTOL;

    public Pistol(TextureRegion decorumTexture, TextureRegion weaponTexture, Vector3 position) {
        //TODO actual textures
        super(decorumTexture,
                weaponTexture,
                AMMO, COOLDOWN, WEAPON_TYPE,
                position);
    }

    @Override
    protected Projectile produceProjectile(double angleOfFire) {
        return new LowCaliberBullet(super.owner.getActiveCell(),
                SPEED,
                angleOfFire,
                DAMAGE,
                new Vector3(super.owner.getPosition().x, super.owner.getPosition().y, 0));
    }
}
