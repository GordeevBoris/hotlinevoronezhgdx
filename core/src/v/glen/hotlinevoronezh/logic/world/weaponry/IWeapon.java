package v.glen.hotlinevoronezh.logic.world.weaponry;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import v.glen.hotlinevoronezh.logic.ballistics.Projectile;

import java.util.List;

/**
 * Created by Boris on 9/6/2016.
 */
public interface IWeapon {

    float DRAWING_OFFSET = 10f;

    Projectile shoot(List<Projectile> globalProjectiles, double angleOfFire);

    void renderWeapon(SpriteBatch batch, double angle);

    WeaponType getWeaponType();

    int getAmmo();

}
