package v.glen.hotlinevoronezh.logic.world.weaponry;

/**
 * Created by Boris on 9/6/2016.
 */
public class NoOwnerException extends RuntimeException {

    public NoOwnerException() {
        super("Weapon was shot without being picked up by owner.");
    }
}
