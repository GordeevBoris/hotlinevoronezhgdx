package v.glen.hotlinevoronezh.logic.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.btree.BehaviorTree;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.ballistics.TrajectoryLine;
import v.glen.hotlinevoronezh.logic.generation.Cell;

/**
 * Created by noire_000 on 8/2/2016.
 */
public abstract class Adversary {

    private static long globalId = 0;

    private final double speed;
    private double health;
    protected final Vector3 position;
    private BehaviorTree tree;
    private final long id;
    private Cell cell;

    protected Adversary(double health,
                        double speed,
                        Vector3 position,
                        Cell cell) {
        this.health = health;
        this.speed = speed;
        this.position = position;
        this.cell = cell;
        id = globalId;
        globalId++;
    }

    public void update() {
        tree.step();
    }

    public abstract void render(SpriteBatch batch, AssetManager assetManager);

    public double getHealth() {
        return health;
    }

    public double getSpeed() {
        return speed;
    }

    public Vector3 getPosition() {
        return position;
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        if (this.cell != null) {
            this.cell.getAdversaries().remove(this);
        }
        this.cell = cell;
        if (this.cell != null) {
            this.cell.getAdversaries().add(this);
        }
    }

    public void destroy() {
        getCell().getDecorum().add(new Corpse(this));
        setCell(null);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }
        if (!(o instanceof Adversary)) {
            return false;
        }
        Adversary adversary = (Adversary) o;
        return id == adversary.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    protected void setTree(BehaviorTree tree) {
        this.tree = tree;
    }

    public abstract double getRadius();

    public abstract Texture getCorpseTexture(AssetManager assetManager);

    public void setHealth(double health) {
        this.health = health;
    }
}
