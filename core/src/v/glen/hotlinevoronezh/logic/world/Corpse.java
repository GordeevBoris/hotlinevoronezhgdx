package v.glen.hotlinevoronezh.logic.world;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Boris on 8/19/2016.
 */
public class Corpse implements IDecorum {

    private final Adversary adversary;

    public Corpse(Adversary adversary) {
        this.adversary = adversary;
    }

    @Override
    public void render(SpriteBatch batch, AssetManager manager) {
        batch.draw(adversary.getCorpseTexture(manager),
                adversary.getPosition().x - (float)adversary.getRadius(),
                adversary.getPosition().y - (float)adversary.getRadius(),
                (float)adversary.getRadius()*2,
                (float)adversary.getRadius()*2);
    }

    @Override
    public void renderOcclusive(SpriteBatch batch, AssetManager manager) {}

    @Override
    public boolean isOcclusive() {
        return false;
    }
}
