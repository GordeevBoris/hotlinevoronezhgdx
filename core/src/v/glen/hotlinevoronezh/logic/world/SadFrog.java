package v.glen.hotlinevoronezh.logic.world;

import com.badlogic.gdx.ai.btree.BehaviorTree;
import com.badlogic.gdx.ai.btree.branch.DynamicGuardSelector;
import com.badlogic.gdx.ai.btree.branch.Selector;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.ballistics.BouncyTrajectoryMethod;
import v.glen.hotlinevoronezh.logic.behavior.FollowTask;
import v.glen.hotlinevoronezh.logic.behavior.WanderTask;
import v.glen.hotlinevoronezh.logic.generation.Cell;

import java.util.List;

/**
 * Created by Boris on 8/9/2016.
 */
public class SadFrog extends Adversary {

    private static final int MOB_SIZE = 50;

    private Texture sadFrog, corpse;

    public SadFrog(double health, double speed, Vector3 position, Cell cell, MainCharacter enemy) {
        super(health, speed, position, cell);
        super.setTree(generateTree(this, enemy));
        sadFrog = null;
    }

    private static BehaviorTree<SadFrog> generateTree(SadFrog host, MainCharacter enemy) {
        //TODO something here makes them behave weird sometimes
        BehaviorTree<SadFrog> tree = new BehaviorTree<>();
        DynamicGuardSelector<SadFrog> selector = new DynamicGuardSelector<>();
        tree.addChild(selector);
        FollowTask<SadFrog> followTask = new FollowTask<>(enemy, host);
        WanderTask<SadFrog> wanderTask = new WanderTask<>(host);
        selector.addChild(followTask);
        selector.addChild(wanderTask);
        return tree;
    }

    @Override
    public void render(SpriteBatch batch, AssetManager assetManager) {
        if (sadFrog == null) {
            sadFrog = assetManager.get("sadfrog.png", Texture.class);
        }
        batch.draw(sadFrog, position.x - MOB_SIZE/2,
                position.y - MOB_SIZE/2, MOB_SIZE, MOB_SIZE);
    }

    @Override
    public double getRadius() {
        return MOB_SIZE/2;
    }

    @Override
    public Texture getCorpseTexture(AssetManager assetManager) {
        if (corpse == null) {
            corpse = assetManager.get("bloodsplats_0003.png", Texture.class);
        }
        return corpse;
    }
}
