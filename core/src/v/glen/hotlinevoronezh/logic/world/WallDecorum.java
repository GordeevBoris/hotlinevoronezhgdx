package v.glen.hotlinevoronezh.logic.world;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.generation.Cell;

/**
 * Created by Boris on 8/28/2016.
 */
public class WallDecorum implements IDecorum {

    public static final float THICKNESS = 5.0f;

    private final Vector3 start, end, character;
    private final float offset;

    public WallDecorum(Vector3 start, Vector3 end, boolean offset, Vector3 character) {
        this.start = start;
        this.end = end;
        this.offset = offset ? THICKNESS : 0;
        this.character = character;
    }

    @Override
    public void render(SpriteBatch batch, AssetManager manager) {
        renderOcclusive(batch, manager);
    }

    @Override
    public void renderOcclusive(SpriteBatch batch, AssetManager manager) {
        if (!floatEquals(start.x, end.x)) {
            batch.draw(manager.get("BlackPixel.png", Texture.class),
                    start.x, start.y - offset,
                    end.x - start.x,
                    THICKNESS);
        } else {
            batch.draw(manager.get("BlackPixel.png", Texture.class),
                    start.x - offset, start.y,
                    THICKNESS,
                    end.y - start.y);
        }
    }

    public void moveAwayFrom(Vector3 position, Cell cell) {
        if (floatEquals(start.x, end.x) && Math.abs(position.x - start.x) < THICKNESS) {
            if (floatEquals(cell.getRectangle().x, start.x)) {
                position.x += THICKNESS;
            }
            if (floatEquals(cell.getRectangle().x + cell.getRectangle().width, start.x)) {
                position.x -= THICKNESS;
            }
        } else if (floatEquals(start.y, end.y) && Math.abs(position.y - start.y) < THICKNESS) {
            if (floatEquals(cell.getRectangle().y, start.y)) {
                position.y += THICKNESS;
            }
            if (floatEquals(cell.getRectangle().y + cell.getRectangle().height, start.y)) {
                position.y -= THICKNESS;
            }
        }
    }

    @Override
    public boolean isOcclusive() {
        return true;
    }

    private boolean floatEquals(float v1, float v2) {
        return Math.abs(v1 - v2) < 0.01f;
    }
}
