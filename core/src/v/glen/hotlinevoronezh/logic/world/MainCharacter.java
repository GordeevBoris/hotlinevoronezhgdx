package v.glen.hotlinevoronezh.logic.world;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.ballistics.ITrajectoryMethod;
import v.glen.hotlinevoronezh.logic.ballistics.Projectile;
import v.glen.hotlinevoronezh.logic.ballistics.ProjectileFrameResult;
import v.glen.hotlinevoronezh.logic.generation.Cell;
import v.glen.hotlinevoronezh.logic.world.weaponry.IWeapon;
import v.glen.hotlinevoronezh.logic.world.weaponry.IWeaponPickup;

import java.util.List;

/**
 * Created by noire_000 on 8/3/2016.
 */
public class MainCharacter implements IDecorum {

    private static final ITrajectoryMethod MOVING_METHOD =
            ITrajectoryMethod.SLIPPERY_TRAJECTORY_METHOD;

    private final Vector3 position;
    private Cell activeCell;
    private final double speed;
    private double health, lastAngle;
    private IWeapon weapon;
    private final Texture gg;

    public MainCharacter(Vector3 position, Cell activeCell, double speed, Texture gg) {
        this.position = position;
        this.activeCell = activeCell;
        this.speed = speed;
        this.gg = gg;
        lastAngle = 0;
        health = 30;
    }

    public void move(double angleDirection) {
        ProjectileFrameResult result = MOVING_METHOD.calculateFrameTrajectory(
                activeCell,
                speed,
                angleDirection,
                position,
                null
        );
        position.set(result.getFinalPosition().x, result.getFinalPosition().y, result.getFinalPosition().z);
        activeCell = result.getFinalCell();
        moveAwayFromWalls();
        if (weapon == null) {
            for (IDecorum decorum : activeCell.getDecorum()) {
                if (IWeaponPickup.class.isAssignableFrom(decorum.getClass()) &&
                        distance(((IWeaponPickup)decorum).getPosition(), position) < IWeaponPickup.PICKUP_RADIUS) {
                    weapon = ((IWeaponPickup)decorum).pickup(this);
                    break;
                }
            }
            if (weapon != null) {
                activeCell.getDecorum().remove(weapon);
            }
        }
    }

    private static double distance(Vector3 p1, Vector3 p2) {
        return Math.sqrt(
                (p2.x - p1.x) * (p2.x - p1.x)
                + (p2.y - p1.y) * (p2.y - p1.y)
        );
    }

    private void moveAwayFromWalls() {
        for (IDecorum decorum : activeCell.getDecorum()) {
            if (WallDecorum.class.isAssignableFrom(decorum.getClass())) {
                ((WallDecorum)decorum).moveAwayFrom(position, activeCell);
            }
        }
    }

    public Vector3 getPosition() {
        return position;
    }

    public Cell getActiveCell() {
        return activeCell;
    }

    public double getHealth() {
        return health;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public void shoot(List<Projectile> globalProjectiles, double angle) {
        lastAngle = angle;
        if (weapon != null) {
            weapon.shoot(globalProjectiles, angle);
            if (weapon.getAmmo() <= 0) {
                weapon = null;
            }
        }
    }

    public void setWeapon(IWeapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public void render(SpriteBatch batch, AssetManager manager) {
        batch.draw(gg, position.x - gg.getWidth()/2, position.y - gg.getHeight()/2);
        if (weapon != null) {
            weapon.renderWeapon(batch, lastAngle);
        }
    }

    @Override
    public void renderOcclusive(SpriteBatch batch, AssetManager manager) {}

    @Override
    public boolean isOcclusive() {
        return false;
    }
}
