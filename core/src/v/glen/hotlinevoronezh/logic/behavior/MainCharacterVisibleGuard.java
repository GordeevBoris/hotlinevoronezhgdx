package v.glen.hotlinevoronezh.logic.behavior;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.btree.LeafTask;
import com.badlogic.gdx.ai.btree.Task;
import com.badlogic.gdx.math.Vector3;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import v.glen.hotlinevoronezh.collections.GdxArrayListAdapter;
import v.glen.hotlinevoronezh.logic.ballistics.*;
import v.glen.hotlinevoronezh.logic.world.Adversary;
import v.glen.hotlinevoronezh.logic.world.MainCharacter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Boris on 8/15/2016.
 */
public class MainCharacterVisibleGuard<T extends Adversary> extends LeafTask<T> {

    private static final NonBouncyTrajectoryMethod LOOKING_METHOD =
            ITrajectoryMethod.NON_BOUNCY_TRAJECTORY_METHOD;

    private final MainCharacter target;
    private final int visibleRange;
    private final Adversary adversary;
    private final List<TrajectoryLine> result;

    public MainCharacterVisibleGuard(MainCharacter target, int visibleRange,
                                     Adversary adversary) {
        this.target = target;
        this.visibleRange = visibleRange;
        this.adversary = adversary;
        result = new GdxArrayListAdapter<>(10, true);
    }

    @Override
    public Status execute() {
        double distance = Math.sqrt(
                (target.getPosition().x - adversary.getPosition().x) * (target.getPosition().x - adversary.getPosition().x)
                + (target.getPosition().y - adversary.getPosition().y) * (target.getPosition().y - adversary.getPosition().y)
        );
        if (distance > visibleRange) {
            return Status.FAILED;
        }
        double targetX = target.getPosition().x - adversary.getPosition().x,
                targetY = target.getPosition().y - adversary.getPosition().y,
                targetAngle = Math.toDegrees(Math.atan2(targetY, targetX));
        if (targetAngle < 0) {
            targetAngle += 360;
        }
        ProjectileFrameResult frame = LOOKING_METHOD.calculateFrameTrajectory(
                adversary.getCell(),
                distance,
                targetAngle,
                adversary.getPosition(),
                null
        );
        if (frame.getFinalCell().equals(target.getActiveCell())
            && Math.abs(frame.getFinalPosition().x - target.getPosition().x) < 1
            && Math.abs(frame.getFinalPosition().y - target.getPosition().y) < 1) {
            result.clear();
            result.addAll(frame.getLines());
            return Status.SUCCEEDED;
        } else {
            return Status.FAILED;
        }
    }

    @Override
    protected Task<T> copyTo(Task<T> task) {
        throw new NotImplementedException();
    }

    public List<TrajectoryLine> getResult() {
        return result;
    }
}
