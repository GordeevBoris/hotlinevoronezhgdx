package v.glen.hotlinevoronezh.logic.behavior;

import com.badlogic.gdx.ai.btree.LeafTask;
import com.badlogic.gdx.ai.btree.Task;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import v.glen.hotlinevoronezh.logic.ballistics.TrajectoryLine;
import v.glen.hotlinevoronezh.logic.world.Adversary;
import v.glen.hotlinevoronezh.logic.world.MainCharacter;

/**
 * Created by Boris on 8/13/2016.
 */
public class FollowTask<T extends Adversary> extends LeafTask<T> {

    private final Adversary adversary;
    private final MainCharacterVisibleGuard<T> guard;

    public FollowTask(MainCharacter target, Adversary adversary) {
        this.adversary = adversary;
        guard = new MainCharacterVisibleGuard<>(target, 1000, adversary);
        this.setGuard(guard);
    }

    @Override
    public Status execute() {
        double resultSpeed = adversary.getSpeed(), distance;
        for (TrajectoryLine line : guard.getResult()) {
            distance = Math.sqrt(
                    (line.getEnd().x - line.getStart().x) * (line.getEnd().x - line.getStart().x)
                            + (line.getEnd().y - line.getStart().y) * (line.getEnd().y - line.getStart().y)
            );
            if (distance < resultSpeed) {
                resultSpeed -= distance;
            } else {
                double coefficient = resultSpeed / distance,
                        endX = line.getStart().x + (line.getEnd().x - line.getStart().x) * coefficient,
                        endY = line.getStart().y + (line.getEnd().y - line.getStart().y) * coefficient;
                if (!adversary.getCell().getRectangle().contains((float)endX, (float)endY)) {
                    if (line.getEndCell().getRectangle().contains((float)endX, (float)endY)) {
                        adversary.setCell(line.getEndCell());
                        adversary.getPosition().set((float)endX, (float)endY, 0);
                    }
                } else {
                    adversary.getPosition().set((float)endX, (float)endY, 0);
                }
                break;
            }
        }

        return Status.RUNNING;
    }

    @Override
    protected Task<T> copyTo(Task<T> task) {
        throw new NotImplementedException();
    }
}
