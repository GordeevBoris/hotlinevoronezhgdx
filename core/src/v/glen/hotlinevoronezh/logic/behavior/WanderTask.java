package v.glen.hotlinevoronezh.logic.behavior;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.btree.LeafTask;
import com.badlogic.gdx.ai.btree.Task;
import com.badlogic.gdx.math.Vector2;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import v.glen.hotlinevoronezh.collections.GdxArrayListAdapter;
import v.glen.hotlinevoronezh.logic.ballistics.ITrajectoryMethod;
import v.glen.hotlinevoronezh.logic.ballistics.ProjectileFrameResult;
import v.glen.hotlinevoronezh.logic.ballistics.TrajectoryLine;
import v.glen.hotlinevoronezh.logic.generation.Cell;
import v.glen.hotlinevoronezh.logic.world.Adversary;

import java.util.List;
import java.util.Random;

/**
 * Created by Boris on 8/13/2016.
 */
public class WanderTask<T extends Adversary> extends LeafTask<T> {

    private static final int WANDER_WAIT_FRAMES = 360;
    private static final int WANDER_RUN_FRAMES = 360;
    private static final float WANDER_SPEED_COEFFICIENT = 0.5f;

    private static final ITrajectoryMethod WANDERING_METHOD
            = ITrajectoryMethod.BOUNCY_TRAJECTORY_METHOD;

    private final Adversary blackboard;
    private final List<TrajectoryLine> trajectory;
    private boolean wait;
    private int framesLeft;

    public WanderTask(T blackboard) {
        this.blackboard = blackboard;
        this.trajectory = new GdxArrayListAdapter<>(10, true);;
    }

    @Override
    public void start() {
        super.start();
        wait = false;
        framesLeft = WANDER_RUN_FRAMES;
    }

    @Override
    public void end() {
        super.end();
    }

    @Override
    public Status execute() {
        if (!wait) {
            if (trajectory.size() <= 0) {
                trajectory.addAll(WANDERING_METHOD.calculateFrameTrajectory(
                        blackboard.getCell(),
                        blackboard.getSpeed() * WANDER_RUN_FRAMES * WANDER_SPEED_COEFFICIENT,
                        new Random().nextFloat() * 360.0f,
                        blackboard.getPosition(),
                        null
                ).getLines());
            }
            int framesPassed = WANDER_RUN_FRAMES - framesLeft;
            double distancePassed = blackboard.getSpeed() * WANDER_SPEED_COEFFICIENT * framesPassed;
            for (TrajectoryLine line : trajectory) {
                double distance = Math.sqrt(
                        (line.getEnd().x - line.getStart().x) * (line.getEnd().x - line.getStart().x)
                        + (line.getEnd().y - line.getStart().y) * (line.getEnd().y - line.getStart().y)
                );
                if (distancePassed > distance) {
                    distancePassed -= distance;
                } else {
                    double coefficient = distancePassed / distance,
                        endX = line.getStart().x + (line.getEnd().x - line.getStart().x) * coefficient,
                        endY = line.getStart().y + (line.getEnd().y - line.getStart().y) * coefficient;
                    blackboard.getPosition().set((float)endX, (float)endY, 0);
                    if (!blackboard.getCell().getRectangle().contains((float)endX, (float)endY)) {
                        blackboard.setCell(line.getEndCell());
                    }
                    break;
                }
            }
        }
        framesLeft--;
        if (framesLeft <= 0) {
            framesLeft = wait ? WANDER_RUN_FRAMES : WANDER_WAIT_FRAMES;
            wait = !wait;
            trajectory.clear();
            return Status.SUCCEEDED;
        } else {
            return Status.RUNNING;
        }
    }

    @Override
    protected Task<T> copyTo(Task<T> task) {
        throw new NotImplementedException();
    }
}
