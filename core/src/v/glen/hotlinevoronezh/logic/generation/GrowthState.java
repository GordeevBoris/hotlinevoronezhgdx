package v.glen.hotlinevoronezh.logic.generation;

import java.util.List;

/**
 * Created by noire_000 on 7/30/2016.
 */
public class GrowthState implements ICellEngineState {

    private boolean done = false;
    private int current = 0;

    @Override
    public boolean done() {
        return done;
    }

    @Override
    public void tick(List<Cell> cells) {
        if (done) {
            return;
        }
        Cell cell = cells.get(current);
        cell.getRectangle().setX(cell.getRectangle().getX() * Field.growthRatio);
        cell.getRectangle().setY(cell.getRectangle().getY() * Field.growthRatio);
        cell.getRectangle().setWidth(cell.getRectangle().getWidth() * Field.growthRatio);
        cell.getRectangle().setHeight(cell.getRectangle().getHeight() * Field.growthRatio);
        current++;
        if (current >= cells.size()) {
            done = true;
        }
    }

}
