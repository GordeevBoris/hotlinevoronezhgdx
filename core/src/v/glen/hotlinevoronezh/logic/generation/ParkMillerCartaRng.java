package v.glen.hotlinevoronezh.logic.generation;

import java.util.Random;

/**
 * Created by noire_000 on 7/5/2016.
 */
public class ParkMillerCartaRng {

    public int seed;
    private int walkingNumber;

    public ParkMillerCartaRng(int seed) {
        this.seed = seed;
        walkingNumber = seed;
    }

    public ParkMillerCartaRng() {
        this.seed = new Random().nextInt();
        walkingNumber = seed;
    }

    public void reset() {
        walkingNumber = seed;
    }

    public int generate() {
        int hi, lo;
        lo = 16807 * (walkingNumber & 0xFFFF);
        hi = 16807 * (walkingNumber >>> 16);
        lo += (hi & 0x7FFF) << 16;
        lo += hi >>> 15;
        lo = (lo & 0x7FFFFFFF) + (lo >>> 31);
        walkingNumber = lo;
        return walkingNumber;
    }

    public double generatePercent() {
        return ((double)(generate()& 0xffffffffL) / 0x7FFFFFFF);
    }

    public long generateRanged(long min, long max) {
        return min + (long)(generatePercent() * (max - min));
    }

    public long generateOffseted(long previous, double offsetTimes, long ceil, long floor) {
        long min = (long)Math.min(previous + (previous * offsetTimes), (double)ceil);
        long max = (long)Math.max(previous - (previous * offsetTimes), (double)floor);
        return generateRanged(min, max);
    }

    public int getSeed() {
        return seed;
    }

}
