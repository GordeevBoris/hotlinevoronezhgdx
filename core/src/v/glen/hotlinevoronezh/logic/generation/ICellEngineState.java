package v.glen.hotlinevoronezh.logic.generation;

import java.util.List;

/**
 * Created by noire_000 on 7/5/2016.
 */
public interface ICellEngineState {

    boolean done();

    void tick(List<Cell> Cells);

}
