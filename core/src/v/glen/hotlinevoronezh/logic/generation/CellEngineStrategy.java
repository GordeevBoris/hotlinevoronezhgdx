package v.glen.hotlinevoronezh.logic.generation;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Created by noire_000 on 7/5/2016.
 */
public class CellEngineStrategy {

    private List<Cell> cells;
    private Queue<ICellEngineState> program;
    private ICellEngineState state = null;
    private boolean done = false;

    public CellEngineStrategy(Queue<ICellEngineState> program) {
        this.program = program;
        cells = new ArrayList<>(Field.fieldCellCap + 1);
        state = program.poll();
    }

    public void tick() {
        if (done) {
            return;
        }
        state.tick(cells);
        if (state.done() && program.size() > 0) {
            state = program.poll();
        } else if (state.done() && program.size() == 0) {
            done = true;
        }
    }

    public List<Cell> getCells() {
        return new ArrayList<>(cells);
    }

    public boolean isDone() {
        return done;
    }
}
