package v.glen.hotlinevoronezh.logic.generation;

import com.badlogic.gdx.math.Rectangle;

import java.util.List;

/**
 * Created by noire_000 on 7/18/2016.
 */
public class LinkingState implements ICellEngineState {

    private boolean done = false;
    private int current = 0;

    @Override
    public boolean done() {
        return done;
    }

    @Override
    public void tick(List<Cell> cells) {
        if (done) {
            return;
        }
        Cell from = cells.get(current);
        current++;
        if (current == cells.size()) {
            done = true;
            return;
        }
        for (int i = current; i < cells.size(); i++) {
            Cell away = cells.get(i);
            if (from.getRectangle().overlaps(away.getRectangle()) ||
                    touches(from.getRectangle(), away.getRectangle())) {
                from.getAdjacent().add(away);
                away.getAdjacent().add(from);
            }
        }
    }

    private boolean touches(Rectangle from, Rectangle away) {
        return (touchesWithinDimension(from.getX(), away.getX(), from.getWidth(), away.getWidth())
                    && withinReachDimensionSymmetric(from.getY(), away.getY(), from.getHeight(), away.getHeight()))
                || (touchesWithinDimension(from.getY(), away.getY(), from.getHeight(), away.getHeight())
                    && withinReachDimensionSymmetric(from.getX(), away.getX(), from.getWidth(), away.getWidth()));
    }

    private boolean withinReachDimensionSymmetric(float fromDimension,
                                                  float awayDimension,
                                                  float fromWidthHeight,
                                                  float awayWidthHeight) {
        return withinReachDimension(fromDimension, awayDimension, fromWidthHeight, awayWidthHeight)
                || withinReachDimension(awayDimension, fromDimension, awayWidthHeight, fromWidthHeight);
    }

    private boolean touchesWithinDimension(float fromDimension,
                                          float awayDimension,
                                          float fromWidthHeight,
                                          float awayWidthHeight) {
        return floatEquals(fromDimension, awayDimension + awayWidthHeight, 0.001f)
                || floatEquals(fromDimension + fromWidthHeight, awayDimension, 0.001f);
    }

    private boolean withinReachDimension(float fromDimension,
                                        float awayDimension,
                                        float fromWidthHeight,
                                        float awayWidthHeight) {
        return (fromDimension < awayDimension + awayWidthHeight
                    && fromDimension > awayDimension)
                || (fromDimension + fromWidthHeight > awayDimension
                    && fromDimension + fromWidthHeight < awayDimension + awayWidthHeight);
    }

    private boolean floatEquals(float f1, float f2, float delta) {
        return Math.abs(f1 - f2) < delta;
    }
}
