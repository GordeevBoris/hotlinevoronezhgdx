package v.glen.hotlinevoronezh.logic.generation;

import com.badlogic.gdx.math.Vector2;

import java.util.*;

/**
 * Created by noire_000 on 7/5/2016.
 */
public class SeparationState implements ICellEngineState {

    private boolean done = false;
    private Map<Long, Boolean> processed = new HashMap<Long, Boolean>();

    @Override
    public boolean done() {
        return done;
    }

    @Override
    public void tick(final List<Cell> cells) {
        if (done) {
            return;
        }
        Comparator<Cell> comparator = new DistanceComparator(Vector2.Zero);
        PriorityQueue<Cell> sorted = new PriorityQueue<>(cells.size(), comparator);
        Cell from = null;
        sorted.addAll(cells);
        for (Cell cell : sorted) {
            if (!processed.containsKey(cell.getId()) || !processed.get(cell.getId())) {
                from = cell;
                break;
            }
        }
        if (from == null) {
            done = true;
            return;
        }
        boolean separated;
        for (Cell away : cells) {
            if (away.getId() != from.getId() &&
                    (!processed.containsKey(away.getId()) || !processed.get(away.getId())) &&
                    from.getRectangle().overlaps(away.getRectangle())) {
                doSeparate(away, from);
                separated = true;
                while (separated) {
                    separated = false;
                    for (Cell from2 : cells) {
                        if (from2.getId() != away.getId()
                                && (processed.containsKey(from2.getId()) && processed.get(from2.getId()))
                                && from2.getRectangle().overlaps(away.getRectangle())) {
                            doSeparate(away, from2);
                            separated = true;
                        }
                    }
                }
            }
        }
        processed.put(from.getId(), true);
    }

    private void doSeparate(Cell away, Cell from) {
        double x1Normalized = away.getRectangle().getCenter(Vector2.Zero).x,
                y1Normalized = away.getRectangle().getCenter(Vector2.Zero).y,
//                x2Normalized = from.getRectangle().getCenter(Vector2.Zero).x,
//                y2Normalized = from.getRectangle().getCenter(Vector2.Zero).y,
                awayInnerVertical = unsignedMin(x1Normalized - away.getRectangle().getWidth()/2,
                        x1Normalized + away.getRectangle().getWidth()/2),
                awayInnerHorizontal = unsignedMin(y1Normalized - away.getRectangle().getHeight()/2,
                        y1Normalized + away.getRectangle().getHeight()/2),
                fromOuterVertical = findFromOuterSide(awayInnerVertical, x1Normalized,
                        away.getRectangle().getWidth(), from.getRectangle().x, from.getRectangle().getWidth()),
                fromOuterHorizontal = findFromOuterSide(awayInnerHorizontal, y1Normalized,
                        away.getRectangle().getHeight(), from.getRectangle().y, from.getRectangle().getHeight()),
                verticalOffset = fromOuterHorizontal - awayInnerHorizontal,
                horizontalOffset = fromOuterVertical - awayInnerVertical,
                horizontalAwayX = x1Normalized + horizontalOffset,
                horizontalAwayY = y1Normalized * horizontalAwayX / x1Normalized,
                verticalAwayY = y1Normalized + verticalOffset,
                verticalAwayX = x1Normalized * verticalAwayY / y1Normalized;
        if (Math.sqrt((x1Normalized - horizontalAwayX) * (x1Normalized - horizontalAwayX) +
                    (y1Normalized - horizontalAwayY) * (y1Normalized - horizontalAwayY))
                < Math.sqrt((x1Normalized - verticalAwayX) * (x1Normalized - verticalAwayX) +
                    (y1Normalized - verticalAwayY) * (y1Normalized - verticalAwayY))) {
            double xOffset = horizontalAwayX - x1Normalized,
                    yOffset = horizontalAwayY - y1Normalized;
            away.move(Math.round(xOffset), Math.round(yOffset));
        } else {
            double xOffset = verticalAwayX - x1Normalized,
                    yOffset = verticalAwayY - y1Normalized;
            away.move(Math.round(xOffset), Math.round(yOffset));
        }
    }

    private double unsignedMin(double val1, double val2) {
        if (Math.abs(val1) < Math.abs(val2)) {
            return val1;
        } else {
            return val2;
        }
    }

    private double findFromOuterSide(double awayInnerSide,
                                     double awayMiddleSide,
                                     double awayWidthHeight,
                                     double fromLeftDownCoord,
                                     double fromWidthHeight) {
        if (awayInnerSide == awayMiddleSide - awayWidthHeight/2) {
            return fromLeftDownCoord + fromWidthHeight;
        } else {
            return fromLeftDownCoord;
        }
    }

}
