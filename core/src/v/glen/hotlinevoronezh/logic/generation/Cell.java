package v.glen.hotlinevoronezh.logic.generation;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import v.glen.hotlinevoronezh.collections.GdxArrayListAdapter;
import v.glen.hotlinevoronezh.logic.ballistics.Projectile;
import v.glen.hotlinevoronezh.logic.world.Adversary;
import v.glen.hotlinevoronezh.logic.world.IDecorum;

import java.util.List;

/**
 * Created by noire_000 on 7/5/2016.
 */
public class Cell {

    private static long globalId = 0;

    private final long id;
    private final Rectangle rectangle;
    private final List<Cell> adjacent;
    private final List<Projectile> projectiles;
    private final List<Adversary> adversaries;
    private final List<IDecorum> decorum;

    public Cell(Rectangle rectangle, long id) {
        this.id = id;
        this.rectangle = rectangle;
        adjacent = new GdxArrayListAdapter<>(2, true);
        projectiles = new GdxArrayListAdapter<>(5, true);
        adversaries = new GdxArrayListAdapter<>(5, true);
        decorum = new GdxArrayListAdapter<>(5, true);
    }

    public Cell(Rectangle rectangle) {
        this(rectangle, globalId);
        globalId++;
    }

    public void move(Vector2 offsetMovement) {
        rectangle.setPosition(rectangle.getPosition(Vector2.Zero).add(offsetMovement));
    }

    public void move(float x, float y) {
        rectangle
                .setX(rectangle.getX() + x)
                .setY(rectangle.getY() + y);
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public long getId() {
        return id;
    }

    public List<Cell> getAdjacent() {
        return adjacent;
    }

    @Override
    public boolean equals(Object that) {
        return (that != null &&
                Cell.class.isAssignableFrom(that.getClass()) &&
                ((Cell)that).getId() == this.getId());
    }

    public List<Projectile> getProjectiles() {
        return projectiles;
    }

    public List<Adversary> getAdversaries() {
        return adversaries;
    }

    public List<IDecorum> getDecorum() {
        return decorum;
    }
}
