package v.glen.hotlinevoronezh.logic.generation;

/**
 * Created by noire_000 on 7/30/2016.
 */
public class GenerationException extends RuntimeException {

//    public GenerationException(String message, Throwable cause,
//                               boolean smth) {
//        super(message, cause, );
//    }

    public GenerationException(String message, Throwable cause) {
        super(message, cause);
    }

    public GenerationException(String message) {
        this(message, null);
    }

    public GenerationException(Throwable cause) {
        this(null, cause);
    }
}
