package v.glen.hotlinevoronezh.logic.generation;

import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.world.WallDecorum;

import java.util.List;

/**
 * Created by Boris on 8/28/2016.
 */
public class WallsAdditionStage implements ICellEngineState {

    private boolean done = false;
    private int current = 0;
    private final Vector3 mainCharacterPosition;

    public WallsAdditionStage(Vector3 mainCharacterPosition) {
        this.mainCharacterPosition = mainCharacterPosition;
    }

    @Override
    public boolean done() {
        return done;
    }

    @Override
    public void tick(List<Cell> cells) {
        if (done) {
            return;
        }
        generateWalls(cells.get(current), cells);
        current++;
        if (current >= cells.size()) {
            done = true;
        }
    }

    private void generateWalls(Cell cell, List<Cell> cells) {
        processLine(cell.getRectangle().x, cell.getRectangle().y, false, cell, true, cells);
        processLine(cell.getRectangle().x, cell.getRectangle().y, true, cell, true, cells);
        processLine(cell.getRectangle().x + cell.getRectangle().width, cell.getRectangle().y, true, cell, false, cells);
        processLine(cell.getRectangle().x, cell.getRectangle().y + cell.getRectangle().height, false, cell, false, cells);
    }

    private void processLine(float x, float y, boolean moveAxisY, Cell cell,
                             boolean offset, List<Cell> cells) {
        float par = moveAxisY ? y : x,
                per = moveAxisY ? x : y,
                end = par + (moveAxisY ? cell.getRectangle().height : cell.getRectangle().width),
                i = WallDecorum.THICKNESS*WallDecorum.THICKNESS;
        Vector3 startingPoint = null;
        while (!floatEquals(par - i, par) && par - i < par) {
            boolean belongsToCell = moveAxisY ? belongsToCell(per, par - i, cells, false) :
                    belongsToCell(par - i, per, cells, true);
            if (!belongsToCell && startingPoint == null) {
                startingPoint = moveAxisY ? new Vector3(per, par - i, 0) : new Vector3(par - i, per, 0);
            }
            if (belongsToCell && startingPoint != null) {
                addWall(startingPoint,
                        moveAxisY ? new Vector3(per, par - i, 0) : new Vector3(par - i, per, 0),
                        cell, offset);
                startingPoint = null;
            }
            i -= 1.0f;
        }
        while (!floatEquals(par, end) && par < end) {
            boolean isAdjacent = moveAxisY ? isAdjacent(per, par, cell, false) :
                    isAdjacent(par, per, cell, true);
            if (!isAdjacent && startingPoint == null) {
                startingPoint = moveAxisY ? new Vector3(per, par, 0) : new Vector3(par, per, 0);
            }
            if (isAdjacent && startingPoint != null) {
                addWall(startingPoint,
                        moveAxisY ? new Vector3(per, par, 0) : new Vector3(par, per, 0),
                        cell, offset);
                startingPoint = null;
            }
            par += 1.0f;
        }
        i = 1.0f;
        while (!floatEquals(i, WallDecorum.THICKNESS*WallDecorum.THICKNESS) && i < WallDecorum.THICKNESS*WallDecorum.THICKNESS) {
            boolean belongsToCell = moveAxisY ? belongsToCell(per, par + i, cells, false) :
                    belongsToCell(par + i, per, cells, true);
            if (!belongsToCell && startingPoint == null) {
                startingPoint = moveAxisY ? new Vector3(per, par + i, 0) : new Vector3(par + i, per, 0);
            }
            if (belongsToCell && startingPoint != null) {
                addWall(startingPoint,
                        moveAxisY ? new Vector3(per, par + i, 0) : new Vector3(par + i, per, 0),
                        cell, offset);
                startingPoint = null;
            }
            i += 1.0f;
        }
        if (startingPoint != null) {
            addWall(startingPoint,
                    moveAxisY ? new Vector3(per, end, 0) : new Vector3(end, per, 0),
                    cell, offset);
        }
    }

    private boolean belongsToCell(float x, float y, List<Cell> cells, boolean moveAlongY) {
        for (Cell cell : cells) {
            if ((cell.getRectangle().x < x &&
                    cell.getRectangle().x + cell.getRectangle().width > x &&
                    cell.getRectangle().y < y &&
                    cell.getRectangle().y + cell.getRectangle().height > y) ||
                    isAdjacent(x, y, cell, moveAlongY)) {
                return true;
            }
        }
        return false;
    }

    private void addWall(Vector3 startingPoint, Vector3 endingPoint, Cell cell, boolean offset) {
        cell.getDecorum().add(new WallDecorum(startingPoint, endingPoint,
                offset, mainCharacterPosition));
    }

    private boolean isAdjacent(float x, float y, Cell cell, boolean MoveAlongX) {
        for (Cell adjacent : cell.getAdjacent()) {
            if (MoveAlongX) {
                if (withinSide(adjacent.getRectangle().x,
                        x,
                        adjacent.getRectangle().y,
                        y,
                        adjacent.getRectangle().width,
                        adjacent.getRectangle().height)) {
                    return true;
                }
            } else {
                if (withinSide(adjacent.getRectangle().y,
                        y,
                        adjacent.getRectangle().x,
                        x,
                        adjacent.getRectangle().height,
                        adjacent.getRectangle().width)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean floatEquals(float v1, float v2) {
        return Math.abs(v1 - v2) < 0.01f;
    }

    private boolean withinSide(float parCoord2,
                               float parCoord1,
                               float perCoord2,
                               float perCoord1,
                               float parLength,
                               float perLength) {
        return  ((parCoord1 > parCoord2 || floatEquals(parCoord1, parCoord2)) &&
                (parCoord1 < parCoord2 + parLength || (floatEquals(parCoord1, parCoord2 + parLength))) &&
                (floatEquals(perCoord1, perCoord2) || floatEquals(perCoord1, perCoord2 + perLength)));
    }
}
