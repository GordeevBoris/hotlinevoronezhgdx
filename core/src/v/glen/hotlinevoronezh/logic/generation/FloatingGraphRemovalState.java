package v.glen.hotlinevoronezh.logic.generation;

import java.util.List;

/**
 * Created by noire_000 on 7/30/2016.
 */
public class FloatingGraphRemovalState implements ICellEngineState {

    private boolean done = false;
    private boolean[] checked;
    private boolean firstTick = true;
    private int current = 0;
    private int startingId;

    public FloatingGraphRemovalState(int startingId) {
        this.startingId = startingId;
    }

    @Override
    public boolean done() {
        return done;
    }

    @Override
    public void tick(List<Cell> cells) {
        if (done) {
            return;
        }
        if (firstTick) {
            firstTick = false;
            checked = new boolean[cells.size()];
            for(int i = 0; i < checked.length; i++) {
                checked[i] = false;
            }
            Cell startingCell = null;
            for(Cell cell : cells) {
                if (cell.getId() == startingId) {
                    startingCell = cell;
                    break;
                }
            }
            if (startingCell == null) {
                throw new GenerationException("Starting cell not found.");
            }
            dfsCheck(startingCell);
        } else {
            if (current >= checked.length) {
                done = true;
                if (cells.size() < Field.minimumAcceptableCellsLeft) {
                    throw new GenerationException("Not enough cells left (bad seed?)");
                }
                return;
            }
            if (!checked[current]) {
                Cell deleteCell = null;
                for(Cell cell : cells) {
                    if (cell.getId() == current) {
                        deleteCell = cell;
                        break;
                    }
                }
                cells.remove(deleteCell);
            }
            current++;
        }
    }

    public void dfsCheck(Cell cell) {
        if (!checked[(int)cell.getId()]) {
            checked[(int)cell.getId()] = true;
            for(Cell adjacent : cell.getAdjacent()) {
                dfsCheck(adjacent);
            }
        }
    }

}
