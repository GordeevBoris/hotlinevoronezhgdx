package v.glen.hotlinevoronezh.logic.generation;

import com.badlogic.gdx.math.Vector2;

import java.util.*;

/**
 * Created by noire_000 on 7/27/2016.
 */
public class CubicSeparationState implements ICellEngineState {

    private boolean done = false;
    private Map<Long, Boolean> processed = new HashMap<Long, Boolean>();

    @Override
    public boolean done() {
        return done;
    }

    @Override
    public void tick(final List<Cell> cells) {
        if (done) {
            return;
        }
        Comparator<Cell> comparator = new DistanceComparator(Vector2.Zero);
        PriorityQueue<Cell> sorted = new PriorityQueue<>(cells.size(), comparator);
        Cell from = null;
        sorted.addAll(cells);
        for (Cell cell : sorted) {
            if (!processed.containsKey(cell.getId()) || !processed.get(cell.getId())) {
                from = cell;
                break;
            }
        }
        if (from == null) {
            done = true;
            return;
        }
        boolean separated;
        for (Cell away : cells) {
            if (away.getId() != from.getId() &&
                    (!processed.containsKey(away.getId()) || !processed.get(away.getId())) &&
                    from.getRectangle().overlaps(away.getRectangle())) {
                doSeparate(away, from);
                separated = true;
                while (separated) {
                    separated = false;
                    for (Cell from2 : cells) {
                        if (from2.getId() != away.getId()
                                && (processed.containsKey(from2.getId()) && processed.get(from2.getId()))
                                && from2.getRectangle().overlaps(away.getRectangle())) {
                            doSeparate(away, from2);
                            separated = true;
                        }
                    }
                }
            }
        }
        processed.put(from.getId(), true);
    }

    private void doSeparate(Cell away, Cell from) {
        double rotation = Math.toDegrees(Math.atan2(away.getRectangle().getCenter(Vector2.Zero).y - from.getRectangle().getCenter(Vector2.Zero).y,
                        away.getRectangle().getCenter(Vector2.Zero).x - from.getRectangle().getCenter(Vector2.Zero).x)) + 45;
        if (rotation < 0) {
            rotation += 360;
        }
        if (rotation > 360) {
            rotation -= 360;
        }
        if (rotation >= 0 && rotation <= 90) {
            moveRight(away, from);
        } else if (rotation >= 90 && rotation <= 180) {
            moveUp(away, from);
        } else if (rotation >= 180 && rotation <= 270) {
            moveLeft(away, from);
        } else {
            moveDown(away, from);
        }
    }

    private void moveRight(Cell away, Cell from) {
        away.move(new Vector2(
                from.getRectangle().x + from.getRectangle().width - away.getRectangle().x,
                0
        ));
    }

    private void moveUp(Cell away, Cell from) {
        away.move(new Vector2(
                0,
                from.getRectangle().y + from.getRectangle().height - away.getRectangle().y
        ));
    }

    private void moveLeft(Cell away, Cell from) {
        away.move(new Vector2(
                from.getRectangle().x - away.getRectangle().x - away.getRectangle().width,
                0
        ));
    }

    private void moveDown(Cell away, Cell from) {
        away.move(new Vector2(
                0,
                from.getRectangle().y - away.getRectangle().y - away.getRectangle().height
        ));
    }

}
