package v.glen.hotlinevoronezh.logic.generation;

import com.badlogic.gdx.math.Vector2;

import java.util.Comparator;

/**
 * Created by noire_000 on 7/9/2016.
 */
public class DistanceComparator implements Comparator<Cell> {

    private final Vector2 center;

    public DistanceComparator(Vector2 center) {
        this.center = center;
    }

    @Override
    public int compare(Cell o1, Cell o2) {
        return (int)Math.ceil(Math.abs(distance(center, o1.getRectangle().getCenter(Vector2.Zero)) -
                distance(center, o2.getRectangle().getCenter(Vector2.Zero))))
                * (int)Math.signum(distance(center, o1.getRectangle().getCenter(Vector2.Zero)) -
                distance(center, o2.getRectangle().getCenter(Vector2.Zero)));
    }

    private double distance(Vector2 P1, Vector2 P2) {
        return Math.sqrt((P1.x - P2.x) * (P1.x - P2.x) + (P1.y - P2.y) * (P1.y - P2.y));
    }
}
