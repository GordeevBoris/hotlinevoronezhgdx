package v.glen.hotlinevoronezh.logic.generation;

import com.badlogic.gdx.math.Rectangle;

import java.util.List;

/**
 * Created by noire_000 on 7/5/2016.
 */
public class SpawnState implements ICellEngineState {

    private boolean done = false;
    private ParkMillerCartaRng rng;
    private int id;

    public SpawnState() {
        this(new ParkMillerCartaRng());
    }

    public SpawnState(ParkMillerCartaRng rng) {
        this.rng = rng;
        id = 0;
    }

    @Override
    public boolean done() {
        return done;
    }

    @Override
    public void tick(List<Cell> Cells) {
        if (done) {
            return;
        }
        if (Cells.size() >= Field.fieldCellCap) {
            Cells.add(0, new Cell(new Rectangle(
                    -Field.maxCellLength/2,
                    -Field.maxCellLength/2,
                    Field.maxCellLength,
                    Field.maxCellLength), id));
            id++;
            done = true;
        } else {
            Cell newCell = produceCell();
            id++;
            Cells.add(newCell);
        }

    }

    private Cell produceCell()
    {
        int x = (int)rng.generateRanged(-Field.distanceFromMiddle,
                Field.distanceFromMiddle);
        int y = (int)rng.generateRanged(-Field.distanceFromMiddle,
                Field.distanceFromMiddle);
        int width = (int)rng.generateRanged(Field.minCellLength, Field.maxCellLength);
        int height = (int)rng.generateOffseted(width, Field.allowedSideDifferenceTimes,
                Field.maxCellLength, Field.minCellLength);
        return new Cell(new Rectangle(x - width/2, y - height/2, width, height), id);
    }

}
