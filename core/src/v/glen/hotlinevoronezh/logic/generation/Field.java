package v.glen.hotlinevoronezh.logic.generation;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by noire_000 on 7/5/2016.
 */
public final class Field {

    private Field() {}

    public static final int fieldWidth = 1920;
    public static final int fieldHeight = 1080;
    public static final int fieldCellCap = 150;
    public static final int minimumAcceptableCellsLeft = fieldCellCap / 2;
    public static final int maxCellLength = 10;
    public static final int minCellLength = 5;
    public static final long distanceFromMiddle = 10;
    public static final long growthRatio = 100;
    public static final long minimapRatio = 10;
    public static final double allowedSideDifferenceTimes = 0.5;

}
