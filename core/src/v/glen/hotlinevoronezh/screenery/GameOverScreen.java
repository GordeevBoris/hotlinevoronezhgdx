package v.glen.hotlinevoronezh.screenery;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import v.glen.hotlinevoronezh.logic.generation.Field;

/**
 * Created by Boris on 8/23/2016.
 */
public class GameOverScreen implements Screen {

    private final ScreenStateMachine callback;
    private Camera guiCamera;
    private BitmapFont font;
    private SpriteBatch batch;
    private int framesLeft;


    public GameOverScreen(ScreenStateMachine callback) {
        this.callback = callback;
        guiCamera = new OrthographicCamera(Field.fieldWidth, Field.fieldHeight);
        guiCamera.position.set(0, 0, 0);
        font = new BitmapFont();
        font.setColor(Color.WHITE);
        font.getData().setScale(6);
        batch = new SpriteBatch();
    }

    @Override
    public void show() {
        framesLeft = 300;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.setProjectionMatrix(guiCamera.combined);
        font.draw(batch, "Game Over", 0, 0);
        batch.end();
        if (framesLeft < 0) {
            callback.switchState(ScreenStateMachine.MAIN_MENU_SCREEN, this);
        } else {
            framesLeft--;
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
    }

}
