package v.glen.hotlinevoronezh.screenery;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import v.glen.hotlinevoronezh.logic.generation.Field;

/**
 * Created by noire_000 on 8/7/2016.
 */
public class LoadingScreen implements Screen {

    private final ScreenStateMachine callback;
    private AssetManager manager;
    private Camera guiCamera;
    private BitmapFont font;
    private SpriteBatch batch;


    public LoadingScreen(ScreenStateMachine callback, AssetManager manager) {
        this.callback = callback;
        this.manager = manager;
        guiCamera = new OrthographicCamera(Field.fieldWidth, Field.fieldHeight);
        guiCamera.position.set(0, 0, 0);
        font = new BitmapFont();
        font.setColor(Color.WHITE);
        font.getData().setScale(6);
        batch = new SpriteBatch();
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        if (manager.update()) {
            callback.loadScreens();
            callback.switchState(ScreenStateMachine.MAIN_MENU_SCREEN, this);
        } else {
            Gdx.gl.glClearColor(0, 0, 0, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            float progress = manager.getProgress() * 100;
            batch.begin();
            batch.setProjectionMatrix(guiCamera.combined);
            font.draw(batch, "Loading progress: " + progress + "%.", 0, 0);
            batch.end();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
    }

    public AssetManager getManager() {
        return manager;
    }

    public void setManager(AssetManager manager) {
        this.manager = manager;
    }

}
