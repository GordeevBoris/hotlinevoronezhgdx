package v.glen.hotlinevoronezh.screenery;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import v.glen.hotlinevoronezh.logic.controls.Button;
import v.glen.hotlinevoronezh.logic.controls.MenuInputProcessor;
import v.glen.hotlinevoronezh.logic.generation.Field;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by noire_000 on 8/5/2016.
 */
public class MainMenuScreen implements Screen {

    private ScreenStateMachine callback;
    private MenuInputProcessor menuInputProcessor;
    private Camera guiCamera;
    private List<Button> buttons;
    private SpriteBatch batch;
    private Texture background, buttonW95;
    private BitmapFont font;

    public MainMenuScreen(ScreenStateMachine callback) {
        this.callback = callback;
        guiCamera = new OrthographicCamera(Field.fieldWidth, Field.fieldHeight);
        guiCamera.position.set(guiCamera.viewportWidth/2, guiCamera.viewportHeight/2, 0);
        guiCamera.update();
        menuInputProcessor = new MenuInputProcessor(buttonsCreate(), guiCamera);

        background = callback.getMainAssetManager().get("Title.png", Texture.class);
        batch = new SpriteBatch();
        buttonW95 = callback.getMainAssetManager().get("Button.png", Texture.class);
        font = new BitmapFont();
        font.setColor(Color.BLACK);
        font.getData().setScale(4);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(menuInputProcessor);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(100, 0, 100, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.setProjectionMatrix(guiCamera.combined);
        batch.draw(background, 0, 0);
        for (Button button : buttons) {
            batch.draw(buttonW95,
                    button.getRectangle().x,
                    button.getRectangle().y,
                    button.getRectangle().getWidth(),
                    button.getRectangle().getHeight());
            font.draw(batch, button.getText(),
                    button.getRectangle().x + 60,
                    button.getRectangle().y + 80);
        }
        batch.end();
        int activeButton = menuInputProcessor.getPressedButton();
        if (activeButton == 0) {
            callback.switchState(ScreenStateMachine.LEVEL_SCREEN, this);
        }
        if (activeButton == 1) {
            Gdx.app.exit();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
    }

    private List<Rectangle> buttonsCreate() {
        buttons = new ArrayList<>();
        List<Rectangle> rectangles = new ArrayList<>();

        Rectangle r1 = new Rectangle(810, guiCamera.viewportHeight - 700, 300, 100);
        rectangles.add(r1);
        buttons.add(new Button(r1, "Start"));

        Rectangle r2 = new Rectangle(810, guiCamera.viewportHeight - 1000, 300, 100);
        rectangles.add(r2);
        buttons.add(new Button(r2, "Exit"));

        return rectangles;
    }
}
