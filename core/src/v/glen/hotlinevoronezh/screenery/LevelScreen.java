package v.glen.hotlinevoronezh.screenery;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import v.glen.hotlinevoronezh.logic.ballistics.LowCaliberBullet;
import v.glen.hotlinevoronezh.logic.ballistics.Projectile;
import v.glen.hotlinevoronezh.logic.ballistics.TrajectoryLine;
import v.glen.hotlinevoronezh.logic.controls.CrossPlatformControlsFactory;
import v.glen.hotlinevoronezh.logic.controls.HvInputProcessor;
import v.glen.hotlinevoronezh.logic.generation.Cell;
import v.glen.hotlinevoronezh.logic.generation.Field;
import v.glen.hotlinevoronezh.logic.generation.GenerationException;
import v.glen.hotlinevoronezh.logic.world.Adversary;
import v.glen.hotlinevoronezh.logic.world.IDecorum;
import v.glen.hotlinevoronezh.logic.world.World;

/**
 * Created by noire_000 on 8/4/2016.
 */
public class LevelScreen implements Screen {

    private static final boolean debugLinesLinks = false;
    private static final boolean debugSeed = true;
    private static final boolean debugCell = true;

    private static final int LIGHT_SIZE = 128;
    private static final float LIGHT_UPSCALE = 1f;
    private static final boolean SOFT_SHADOWS = true;
    private static final boolean LIGHTNING_ADDITIVE = true;

    private SpriteBatch batch, gui;
    private Texture gg, blue, red, black, floor, green, shadowMapTex, worldTexture;
    private OrthographicCamera camera, guiCamera, lightningCamera;
    private HvInputProcessor inputProcessor;
    private TextureRegion lineTexture, occluders, shadowMap1D, worldTextureFlip;
    private BitmapFont font;
    private World world;
    private ScreenStateMachine callback;
    private boolean showMap;
    private FrameBuffer occludersFBO, shadowMapFBO, worldFBO;
    private ShaderProgram shadowMapShader, shadowRendererShader;

    public LevelScreen(ScreenStateMachine callback) {
        this.callback = callback;

        gui = new SpriteBatch();
        font = new BitmapFont();
        font.getData().setScale(2);

        guiCamera = new OrthographicCamera(Field.fieldWidth, Field.fieldHeight);
        inputProcessor = CrossPlatformControlsFactory.create(guiCamera, gui,
                callback.getMainAssetManager());

        batch = new SpriteBatch();
        floor = callback.getMainAssetManager().get("FloorTexture.png", Texture.class);
        floor.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        gg = callback.getMainAssetManager().get("gg.png", Texture.class);

        Pixmap pix1 = new Pixmap(2, 2, Pixmap.Format.RGBA8888);
        pix1.setColor(Color.BLUE);
        pix1.fill();
        blue = new Texture(pix1);

        Pixmap pix2 = new Pixmap(2, 2, Pixmap.Format.RGBA8888);
        pix2.setColor(Color.RED);
        pix2.fill();
        red = new Texture(pix2);

        Pixmap pix3 = new Pixmap(2, 2, Pixmap.Format.RGBA8888);
        pix3.setColor(Color.GREEN);
        pix3.fill();
        green = new Texture(pix3);

        Pixmap pix4 = new Pixmap(2, 2, Pixmap.Format.RGBA8888);
        pix4.setColor(Color.BLACK);
        pix4.fill();
        black = new Texture(pix4);

        camera = new OrthographicCamera(Field.fieldWidth, Field.fieldHeight);

        lineTexture = new TextureRegion();
        lineTexture.setTexture(black);
        lineTexture.setRegion(1,1,1,1);

        occludersFBO = new FrameBuffer(Pixmap.Format.RGBA8888, Field.fieldWidth, Field.fieldHeight, false);
        occluders = new TextureRegion(occludersFBO.getColorBufferTexture());
        occluders.flip(false, true);
        shadowMapFBO = new FrameBuffer(Pixmap.Format.RGBA8888, LIGHT_SIZE, 1, false);

        shadowMapTex = shadowMapFBO.getColorBufferTexture();
        shadowMapTex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        shadowMapTex.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);

        shadowMap1D = new TextureRegion(shadowMapTex);
        shadowMap1D.flip(false, true);

        shadowMapShader = new ShaderProgram(Gdx.files.internal("VertexShader.glsl").readString(),
                Gdx.files.internal("ShadowMap.glsl").readString());
        if (!shadowMapShader.isCompiled()) {
            throw new GenerationException("could not compile shader: " + shadowMapShader.getLog());
        }
        if (shadowMapShader.getLog().length() != 0) {
            Gdx.app.log("GpuShadows", shadowMapShader.getLog());
        }

        shadowRendererShader = new ShaderProgram(Gdx.files.internal("VertexShader.glsl").readString(),
                Gdx.files.internal("ShadowRenderer.glsl").readString());
        if (!shadowRendererShader.isCompiled()) {
            throw new GenerationException("could not compile shader: " + shadowRendererShader.getLog());
        }
        if (shadowRendererShader.getLog().length() != 0) {
            Gdx.app.log("GpuShadows", shadowRendererShader.getLog());
        }

        lightningCamera = new OrthographicCamera(Field.fieldWidth, Field.fieldHeight);

        worldFBO = new FrameBuffer(Pixmap.Format.RGBA8888, Field.fieldWidth, Field.fieldHeight, false);
        worldTexture = worldFBO.getColorBufferTexture();
        worldTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        worldTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);

        worldTextureFlip = new TextureRegion(worldTexture);
        worldTextureFlip.flip(false, true);
    }

    @Override
    public void show() {
        inputProcessor.dispose();
        inputProcessor = CrossPlatformControlsFactory.create(guiCamera, gui,
                callback.getMainAssetManager());
        Gdx.input.setInputProcessor(inputProcessor);
        if (callback.getPreviousScreen() == ScreenStateMachine.MAIN_MENU_SCREEN) {
            world = World.generateWorld(callback.getMainAssetManager());
            showMap = false;
            updateCamera();
        }
        //TODO see if you can load resources up only now.
    }

    @Override
    public void render(float delta) {
        glPrepare();
        processInput();
        processWorld();
        processGui();
        processScreenChange();
    }

    private void processScreenChange() {
        if (world.getAdversaries().size() == 0) {
            callback.switchState(ScreenStateMachine.GAME_OVER_SCREEN, this);
        }
        if (world.getMainCharacter().getHealth() <= 0) {
            callback.switchState(ScreenStateMachine.GAME_OVER_SCREEN, this);
        }
    }

    private void processWorld() {
        if (showMap) {
            updateCamera();
            mainBatchPrepare();
            batch.disableBlending();
            showMinimap();
            mainBatchEnd();
            batch.enableBlending();
        } else {
            updateCamera();
            showLightning();
            showWorld();
        }
        world.processProjectiles();
        world.processAdversaries();
    }

    private void showLightning() {
        if (LIGHTNING_ADDITIVE) {
            batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
        }

        drawLightShader(world.getMainCharacter().getPosition().x,
                world.getMainCharacter().getPosition().y, 1024, LIGHT_UPSCALE, Color.WHITE);

        for (Projectile projectile : world.getProjectiles()) {
            drawLightSprite(projectile.getPosition().x,
                    projectile.getPosition().y, 64, Color.YELLOW);
        }

        if (LIGHTNING_ADDITIVE) {
            batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        }
    }

    private void drawLightSprite(float x, float y, float size, Color color) {
        Color oldColor = batch.getColor();
        batch.setColor(color);
        batch.setShader(null);
        batch.enableBlending();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        batch.draw(callback.getMainAssetManager().get("light.png", Texture.class),
                x - size/2f, y - size/2f, size, size);

        batch.end();
        batch.setColor(oldColor);
    }

    /*
    Code taken from https://gist.github.com/mattdesl/5286905
     */
    private void drawLightShader(float x, float y, float size, float upScale, Color color) {
        occludersFBO.begin();

        Gdx.gl.glClearColor(0f,0f,0f,0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        lightningCamera.setToOrtho(false, size, size);

        lightningCamera.position.x = x;
        lightningCamera.position.y = y;

        lightningCamera.update();

        batch.setProjectionMatrix(lightningCamera.combined);
        batch.setShader(null);
        batch.begin();

        drawOccluders();

        batch.end();

        occludersFBO.end();

        shadowMapFBO.begin();

        Gdx.gl.glClearColor(0f,0f,0f,0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setShader(shadowMapShader);
        batch.begin();
        shadowMapShader.setUniformf("resolution", size, size);
        shadowMapShader.setUniformf("upScale", upScale);

        lightningCamera.setToOrtho(false, Field.fieldWidth, shadowMapFBO.getHeight());
        batch.setProjectionMatrix(lightningCamera.combined);

        batch.draw(occluders.getTexture(), 0, 0, Field.fieldWidth, shadowMapFBO.getHeight());

        batch.end();

        shadowMapFBO.end();

        lightningCamera.setToOrtho(false, Field.fieldWidth, Field.fieldHeight);
        lightningCamera.update();
        batch.setProjectionMatrix(lightningCamera.combined);

        batch.setShader(shadowRendererShader);
        batch.begin();

        shadowRendererShader.setUniformf("resolution", size, size);
        shadowRendererShader.setUniformf("softShadows", SOFT_SHADOWS ? 1f : 0f);

        batch.setColor(color);

        float finalSize = size * upScale;

        batch.draw(shadowMap1D.getTexture(),
                x * shadowMap1D.getRegionX() + camera.viewportWidth/2 - finalSize/2f - (camera.position.x - x),
                y * shadowMap1D.getRegionX() + camera.viewportHeight/2 - finalSize/2f - (camera.position.y - y),
                finalSize, finalSize);

        batch.end();
    }

    private void drawOccluders() {
        for (Cell cell : world.getCells()) {
            for (IDecorum decorum : cell.getDecorum()) {
                if (decorum.isOcclusive()) {
                    decorum.renderOcclusive(batch, callback.getMainAssetManager());
                }
            }
        }
    }

    private void showMinimap() {
        Texture renderTexture;
        for (Cell cell : world.getCells()) {
            if (world.getMainCharacter().getActiveCell().equals(cell)) {
                renderTexture = red;
            } else if (cell.getAdversaries().size() > 0) {
                renderTexture = green;
            } else  {
                renderTexture = blue;
            }
            batch.draw(
                    renderTexture,
                    cell.getRectangle().x / Field.minimapRatio,
                    cell.getRectangle().y / Field.minimapRatio,
                    cell.getRectangle().getWidth() / Field.minimapRatio,
                    cell.getRectangle().getHeight() / Field.minimapRatio
            );
        }
    }

    private void showWorld() {
        worldFBO.begin();
        mainBatchPrepare();
        renderCells();
        processDecorum();
        processProjectiles();
        processAdversaries();
        world.getMainCharacter().render(batch, callback.getMainAssetManager());
        renderDebugLines();
        mainBatchEnd();
        worldFBO.end();
        batch.setBlendFunction(GL20.GL_ZERO, GL20.GL_SRC_COLOR);
        mainBatchPrepare();
        batch.draw(worldTextureFlip,
                camera.position.x - camera.viewportWidth/2f,
                camera.position.y - camera.viewportHeight/2f);
        mainBatchEnd();
    }

    private void processGui() {
        guiBatchPrepare();
        drawGui();
        writeDebugInfo();
        guiBatchEnd();
    }

    private void processAdversaries() {
        for (Adversary adversary : world.getAdversaries()) {
            adversary.render(batch, callback.getMainAssetManager());
        }
    }

    private void processDecorum() {
        for (IDecorum decorum : world.getMainCharacter().getActiveCell().getDecorum()) {
            decorum.render(batch, callback.getMainAssetManager());
        }
        for (Cell cell : world.getMainCharacter().getActiveCell().getAdjacent()) {
            for (IDecorum decorum : cell.getDecorum()) {
                decorum.render(batch, callback.getMainAssetManager());
            }
        }
    }

    private void processProjectiles() {
//        for (Projectile projectile : world.getMainCharacter().getActiveCell().getProjectiles()) {
//            for (TrajectoryLine line : projectile.getLastFrame().getLines()) {
//                drawLine(line.getStart().x,
//                        line.getStart().y,
//                        line.getEnd().x,
//                        line.getEnd().y,
//                        5
//                );
//            }
//        }
//        for (Cell adjacent : world.getMainCharacter().getActiveCell().getAdjacent()) {
//            for (Projectile projectile : adjacent.getProjectiles()) {
//                for (TrajectoryLine line : projectile.getLastFrame().getLines()) {
//                    drawLine(line.getStart().x,
//                            line.getStart().y,
//                            line.getEnd().x,
//                            line.getEnd().y,
//                            5
//                    );
//                }
//            }
//        }
        //TODO render projectiles in their frame method now
        //TODO dry this
    }

    private void glPrepare() {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    private void mainBatchPrepare() {
        batch.setProjectionMatrix(camera.combined);
        batch.setShader(null);
        batch.enableBlending();
        batch.begin();
    }

    private void renderCells() {
        for (Cell cell : world.getCells()) {
            batch.draw(floor, cell.getRectangle().getX(), cell.getRectangle().getY(),
                    cell.getRectangle().getWidth(), cell.getRectangle().getHeight(),
                    0,0, cell.getRectangle().getWidth()/floor.getWidth(),
                    cell.getRectangle().getHeight()/floor.getHeight());
        }
    }

    private void updateCamera() {
        camera.setToOrtho(false, Field.fieldWidth, Field.fieldHeight);
        batch.setProjectionMatrix(camera.combined);
        camera.update();
        if (showMap) {
            Rectangle destination = world.getMainCharacter().getActiveCell()
                    .getRectangle();
            camera.position.x = (destination.x + destination.width/2) / Field.minimapRatio;
            camera.position.y = (destination.y + destination.height/2) / Field.minimapRatio;
        } else {
            camera.position.x = world.getMainCharacter().getPosition().x;
            camera.position.y = world.getMainCharacter().getPosition().y;
        }
        camera.update();
    }

    private void processInput() {
        Double result = inputProcessor.processInputMove();
        if (result != null) {
            world.getMainCharacter().move(result);
        }
        result = inputProcessor.processInputShoot();
        if (result != null) {
            world.getMainCharacter().shoot(world.getProjectiles(), result);
        }
        showMap = inputProcessor.minimapOn();
        if (inputProcessor.showMenu()) {
            callback.switchState(ScreenStateMachine.LEVEL_MENU_SCREEN, this);
        }
    }

    private void mainBatchEnd() {
        batch.end();
        occludersFBO.end();
    }

    private void guiBatchPrepare() {
        gui.setProjectionMatrix(guiCamera.combined);
        gui.begin();
    }

    private void drawGui() {
        inputProcessor.renderGuiElements();
    }

    private void writeDebugInfo() {
        renderSeed();
        renderCellInfo();
    }

    private void guiBatchEnd() {
        gui.end();
    }

    private void renderDebugLines() {
        if (debugLinesLinks) {
            for (Cell from : world.getCells()) {
                for (Cell away : from.getAdjacent()) {
                    drawLine(from.getRectangle().getCenter(Vector2.Zero).x,
                            from.getRectangle().getCenter(Vector2.Zero).y,
                            away.getRectangle().getCenter(Vector2.Zero).x,
                            away.getRectangle().getCenter(Vector2.Zero).y,
                            5);
                }
            }
        }
    }

    private void renderSeed() {
        if (debugSeed) {
            font.draw(gui, "Seed: " + world.getRng().getSeed(), 10, 30);
        }
    }

    private void renderCellInfo() {
        if (debugCell) {
            Vector3 lastPointer = new Vector3()
                    .add(inputProcessor.getLastPointer())
                    .add(camera.position)
                    .add(-Field.fieldWidth/2, -Field.fieldHeight/2, 0);
            Cell found = null;
            for (Cell cell : world.getCells()) {
                if (cell.getRectangle().contains(lastPointer.x, lastPointer.y)) {
                    found = cell;
                    break;
                }
            }
            if (found != null) {
                font.draw(gui, "Id: " + found.getId() + "\n"
                        + "X: " + found.getRectangle().getX() + "\n"
                        + "Y: " + found.getRectangle().getY() + "\n"
                        + "Width: " + found.getRectangle().getWidth() + "\n"
                        + "Height: " + found.getRectangle().getHeight(), 10, 200);
            }
        }
    }

    private void drawLine(float x1, float y1, float x2, float y2, int thickness) {
        float dx = x2-x1;
        float dy = y2-y1;
        float dist = (float)Math.sqrt(dx*dx + dy*dy);
        float rad = (float)Math.atan2(dy, dx);
        batch.draw(lineTexture, x1, y1, 0, 0, dist, thickness, 1, 1, (float)Math.toDegrees(rad));
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        gui.dispose();
        font.dispose();
        inputProcessor.dispose();
        red.dispose();
        blue.dispose();
        green.dispose();
        black.dispose();
        occludersFBO.dispose();
        shadowMapFBO.dispose();
        worldFBO.dispose();
        shadowMapTex.dispose();
        worldTexture.dispose();
        shadowMapShader.dispose();
        shadowRendererShader.dispose();
    }
}
