package v.glen.hotlinevoronezh.screenery;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.Disposable;
import v.glen.hotlinevoronezh.HvGame;

/**
 * Created by noire_000 on 8/5/2016.
 */
public class ScreenStateMachine implements Disposable {

    public static final int MAIN_MENU_SCREEN = 0;
    public static final int LEVEL_SCREEN = 1;
    public static final int LEVEL_MENU_SCREEN = 2;
    public static final int ASSET_LOADING_SCREEN = 3;
    public static final int GAME_OVER_SCREEN = 4;

    private static final int TOTAL_SCREENS = 5;

    private final HvGame callback;
    private final Screen[] screens;
    private int previousScreen = -1;
    private final AssetManager mainAssetManager;
    private boolean screendsLoaded;

    public ScreenStateMachine(HvGame callback, AssetManager mainAssetManager) {
        this.callback = callback;
        this.mainAssetManager = mainAssetManager;
        screendsLoaded = false;
        screens = new Screen[TOTAL_SCREENS];
        screens[ASSET_LOADING_SCREEN] = new LoadingScreen(this, mainAssetManager);
        switchState(ScreenStateMachine.ASSET_LOADING_SCREEN, null);
    }

    public void loadScreens() {
        if (!screendsLoaded) {
            screendsLoaded = true;
            screens[MAIN_MENU_SCREEN] = new MainMenuScreen(this);
            screens[LEVEL_SCREEN] = new LevelScreen(this);
            screens[LEVEL_MENU_SCREEN] = new LevelMenuScreen(this);
            screens[GAME_OVER_SCREEN] = new GameOverScreen(this);
        }
    }

    public void switchState(int nextState, Screen caller){
        if (screendsLoaded) {
            int i = 0;
            for (Screen screen : screens) {
                if (screen.equals(caller)) {
                    previousScreen = i;
                    break;
                }
                i++;
            }
        }
        callback.setScreen(screens[nextState]);
    }

    @Override
    public void dispose() {
        for (Screen screen : screens) {
            screen.dispose();
        }
    }

    public int getPreviousScreen() {
        return previousScreen;
    }

    public AssetManager getMainAssetManager() {
        return mainAssetManager;
    }
}
